<section id="ourCourses">
    <div class="container wow fadeInLeft">
        <!-- Our courses titile -->
        <div class="row">
            <div class="col-lg-12 col-md-12">

            </div>
        </div>
        <!-- End Our courses titile -->
        <!-- Start Our courses content -->
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="ourCourse_content">
                    <ul class="course_nav">
                        @foreach($programmes as $programme)
                            <li>
                                <div class="single_course">
                                    <div class="singCourse_imgarea">
                                        <img src="{{$programme->programme_image_name}}" />
                                        <div class="mask">
                                            <a href="{{ route('single.programme', ['id' => $programme->id]) }}" class="course_more">View Programme</a>
                                        </div>
                                    </div>
                                    <div class="singCourse_content">
                                        <h3 class="singCourse_title"><a href="{{ route('single.programme', ['id' => $programme->id]) }}">{{$programme->programme_name}}</a></h3>
                                    </div>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        <!-- End Our courses content -->
    </div>
</section>