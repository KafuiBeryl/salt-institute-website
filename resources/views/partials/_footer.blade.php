<!--=========== BEGIN FOOTER SECTION ================-->
<section id="footer_main">

    <div class="footer_top">

        <div class="container">
            <div class="row">
                <div class="col-lg-1  col-md-1  col-sm-1"></div>
                <div class="col-lg-3  col-md-3  col-sm-3">
                    <div class="single_footer_widget">
                        <h3>Address</h3>
                        <p style="color:white">{{$address}} </p><hr/><p style="color:white">{{$postal_address}}</p>
                    </div>
                </div>

                <div class="col-lg-1  col-md-1  col-sm-1"></div>

                <div class="col-lg-3  col-md-3 col-sm-3">
                    <div class="single_footer_widget">
                        <h3>Contacts</h3>
                        <ul class="footer_widget_nav" style="color:white">
                            <li> {{$phones}}</li> <hr/>
                            <li><a href="mailto:{{$email}}">{{$email}} </a></li> <hr/>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-1  col-md-1  col-sm-1"></div>

                <div class="col-ld-3  col-md-3 col-sm-3">
                    <div class="single_footer_widget">
                        <h3>Social Links</h3>
                        <ul class="footer_social">
                            <li><a data-toggle="tooltip" data-placement="top" title="Facebook" class="soc_tooltip" href="{{$facebook}}" target="_blank"><i class="fa fa-facebook"></i></a></li>
                            <li><a data-toggle="tooltip" data-placement="top" title="Twitter" class="soc_tooltip"  href="{{$twitter}}" target="_blank"><i class="fa fa-twitter"></i></a></li>
                            <li><a data-toggle="tooltip" data-placement="top" title="Instagram" class="soc_tooltip"  href="{{$instagram}}" target="_blank"><i class="fa fa-instagram"></i></a></li>
                            <li><a data-toggle="tooltip" data-placement="top" title="Linkedin" class="soc_tooltip"  href="{{$linked}}" target="_blank"><i class="fa fa-linkedin"></i></a></li>
{{--                            <li><a data-toggle="tooltip" data-placement="top" title="Youtube" class="soc_tooltip"  href="{{$youtube}}"><i class="fa fa-youtube"></i></a></li>--}}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End footer top area -->

    <!-- Start footer bottom area -->
    <div class="footer_bottom">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="footer_bootomLeft">
                        <p> Copyright &copy; 2019 All Rights Reserved</p>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="footer_bootomRight">
                        <p>Designed by <a href="https://nextinfotechgh.com/" target="_blank" rel="nofollow">Next Infotech Ltd</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End footer bottom area -->
    </footer>
</section>
<!--=========== END FOOTER SECTION ================-->