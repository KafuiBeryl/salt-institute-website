<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <!--===============================================
   Template Design By WpFreeware Team.
   Author URI : http://www.wpfreeware.com/
   ====================================================-->

    <!-- Basic Page Needs
    ================================================== -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Salt Institute : Home</title>

    <!-- Mobile Specific Metas
    ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/png" href="{{asset('favicon.ico')}}"/>

    <!-- CSS
    ================================================== -->
    <!-- Bootstrap css file-->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Font awesome css file-->
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
    <!-- Superslide css file-->
    <link rel="stylesheet" href="{{ asset('css/superslides.css') }}">
    <!-- Slick slider css file -->
    <link href="{{ asset('css/slick.css') }}" rel="stylesheet">

    <!-- Circle counter cdn css file -->
    <link rel='stylesheet prefetch' href='https://cdn.rawgit.com/pguso/jquery-plugin-circliful/master/css/jquery.circliful.css'>
    <!-- smooth animate css file -->
    <link rel="stylesheet" href="{{ asset('css/animate.css') }}">
    <!-- preloader -->
{{--    <link rel="stylesheet" href="{{ asset('css/queryLoader.css') }}" type="text/css" />--}}
    <!-- gallery slider css -->
    <link type="text/css" media="all" rel="stylesheet" href="{{ asset('css/jquery.tosrus.all.css') }}" />
    <!-- Default Theme css file -->
    <link id="switcher" href="{{ asset('css/themes/default-theme.css') }}" rel="stylesheet">
    <!-- Main structure css file -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Google fonts -->
    <link href='http://fonts.googleapis.com/css?family=Merriweather' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Varela' rel='stylesheet' type='text/css'>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
</head>
<body>
<!-- SCROLL TOP BUTTON -->
<a class="scrollToTop" href="#"></a>
<!-- END SCROLL TOP BUTTON -->

<!--=========== BEGIN HEADER SECTION ================-->
<header id="header">
    <!-- BEGIN MENU -->
    <div class="menu_area">
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">  <div class="container">
                <div class="navbar-header">
                    <!-- FOR MOBILE VIEW COLLAPSED BUTTON -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <!-- LOGO -->
                    <!-- TEXT BASED LOGO -->
                    <a class="navbar-brand" href="{{ route('home') }}"><span style="padding-bottom: 10px;"><img src= "{{ asset('images/salt_logo.png') }}" alt="salt_logo" height="55px" width="130px"></span></a>


                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    @yield('header')
                </div>
            </div>
        </nav>
    </div>
    <!-- END MENU -->
</header>
<!--=========== END HEADER SECTION ================-->

@yield('banner')


@yield('body')

<!--=========== BEGIN OUR COURSES SECTION ================-->
@include('partials._courses')
<!--=========== END OUR COURSES SECTION ================-->

<div class="clr"></div>
<!--=========== BEGIN FOOTER SECTION ================-->
@include('partials._footer')
<!--=========== END FOOTER SECTION ================-->

<script src="{{ asset('js/html5shiv.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('js/respond.min.js')}}" type="text/javascript"></script>
<!-- Javascript Files
================================================== -->

<!-- initialize jQuery Library -->
<script src="{{ asset('js/jquery.min.js') }}"></script>
<!-- Preloader js file -->
{{--<script src="{{ asset('js/queryloader2.min.js') }}" type="text/javascript"></script>--}}
<!-- For smooth animatin  -->
<script src="{{ asset('js/wow.min.js') }}"></script>
<!-- Bootstrap js -->
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<!-- Responsify js -->
<script src="{{ asset('js/responsify.min.js') }}"></script>
<!-- slick slider -->
<script src="{{ asset('js/slick.min.js') }}"></script>
<!-- superslides slider -->
<script src="{{ asset('js/jquery.easing.1.3.js') }}"></script>
<script src="{{ asset('js/jquery.animate-enhanced.min.js') }}"></script>
<script src="{{ asset('js/jquery.superslides.min.js') }}" type="text/javascript" charset="utf-8"></script>
<!-- for circle counter -->
<script src='{{ asset('js/jquery.circliful.min.js') }}'></script>
<!-- Gallery slider -->
<script type="text/javascript" language="javascript" src="{{ asset('js/jquery.tosrus.min.all.js') }}"></script>

<!-- Custom js-->
<script src="{{ asset('js/custom.js') }}"></script>
<!--===============================================
Template Design By WpFreeware Team.
Author URI : http://www.wpfreeware.com/
====================================================-->
</body>
</html>
