@component('mail::message')
    <strong>Name    : </strong>{{$message->name}}<br>
    <strong>Subject : </strong>{{$message->subject}}<br>
    <strong>Email   :</strong> {{$message->email}} <br>
    <strong>Message : </strong>
    @component('mail::panel')
        {{$message->message}}

    @endcomponent
@endcomponent
