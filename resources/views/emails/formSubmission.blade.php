@component('mail::message')

    @component('mail::panel')
        <strong>TIN No.    : </strong>{{$message->tin_number}}<br>
        <strong>Name : </strong>{{$message->name}}<br>
        <strong>Gender   :</strong> {{$message->gender}} <br>
        <strong>Title : </strong>  {{$message->title}} <br>
        <strong>Organization    : </strong>{{$message->organization}}<br>
        <strong>Designation/Position : </strong>{{$message->position}}<br>
        <strong>Current duties   :</strong> {{$message->duties}} <br>
        <strong>Address : </strong> {{$message->address}} <br>
        <strong>Mobile/Telephone  :</strong> {{$message->phone}}<br>
        <strong>Email :</strong> {{$message->email}} <br>
        <strong>Course   :</strong> {{$message->course}} <br>
        <strong>Preferred Course Time schedule : </strong> {{$message->preferred_schedule}} <br>
        <strong>Level of education    : </strong>{{$message->qualification}}<br><br>
        <strong>Learning goals | Expectations | Personal input</strong><br><br>
        <strong>1. What is your three main objectives for this course? : </strong>
        @component('mail::panel')<br>
            1. {{$message->goal1}}<br>
            2. {{$message->goal2}}<br>
            3. {{$message->goal3}}
        @endcomponent <br>
        <strong>2. List three of your top expectations   :</strong>
        @component('mail::panel')<br>
            1. {{$message->expectations1}}<br>
            2. {{$message->expectations2}}<br>
            3. {{$message->expectations3}}
        @endcomponent <br>
        <strong>3. List three areas where you can contribute to the success of the course : </strong>
        @component('mail::panel')<br>
            1. {{$message->contribute1}}<br>
            2. {{$message->contribute2}}<br>
            3. {{$message->contribute3}}
        @endcomponent <br>
    @endcomponent

@endcomponent
