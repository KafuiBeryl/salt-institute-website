@extends('layouts.layout')

@section('header')
    <ul id="top-menu" class="nav navbar-nav navbar-right main-nav">
        <li ><a href="{{ route('home') }}">Home</a></li>
        <li><a href="{{ route('about') }}">About Us</a></li>
        <li class="dropdown">
            <a href="{{ route('programme') }}" class="dropdown-toggle" role="button" aria-expanded="false">Programmes<span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
                @foreach($programmes as $programme)
                    <li><a href="{{ route('single.programme', ['id' => $programme->id]) }}">{{$programme->programme_name}}</a></li>
                @endforeach
            </ul>
        </li>

        <li class="dropdown">
            <a  class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Our Staff<span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
                <li><a href="{{route('board')}}">Board</a></li>
                <li><a href="{{route('team')}}">Faculty</a></li>
            </ul>
        </li>
        <li class=" active"><a href="{{route('contact')}}">Contact</a></li>
    </ul>
@endsection

@section('banner')
    <section id="imgBanner_about"></section>
@endsection


@section('body')
    <!--=========== BEGIN CONTACT SECTION ================-->
    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="title_area">
                        <h2 class="title_two">{{$contact_title}}</h2>
                        <span></span>
                        <p>{{$contact_pretext}}</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 col-md-8 col-sm-8">
                    <div class="contact_form wow fadeInLeft">
                        <form action="{{route('contact.post')}}" class="submitphoto_form" role="form" method="post">
                            @csrf
                            @if(count($errors))
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{$error}}</li>
                                    @endforeach
                                </ul>
                            @endif
                            <input name="name" type="text" class="wp-form-control wpcf7-text" placeholder="Your name" required pattern="[a-zA-Z ]{1,30}$" value="{{ old('name') }}">
                            <input name="email" type="email" class="wp-form-control wpcf7-email" placeholder="Email address" required value="{{ old('email') }}">
                            <input name="subject"  required pattern="[a-zA-Z ]{1,30}$"  type="text" class="wp-form-control wpcf7-text" placeholder="Subject">
                            <textarea name="message" class="wp-form-control wpcf7-textarea" cols="30" rows="10" placeholder="What would you like to tell us" required maxlength="500">{{old('message')}}</textarea>
                            <br/>
                            <div class="row form-group">
                                <div class="col-md-8 ">
                                    <label>
                                        <img src="{!! captcha_url() !!}" alt="captcha">
                                    </label>
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('captcha') ? ' has-error' : '' }}">
                                <!-- <label class="col-md-4 control-label">Captcha</label> -->

                                <div >
                                    <input type="text" class="wp-form-control wpcf7-text" placeholder="Captcha" name="captcha" autocomplete="off">

                                    @if ($errors->has('captcha'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('captcha') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <br>
                            <br>
                            <input type="submit" value="Submit" class="wpcf7-submit">
                            <img id="yourLoadingImage" src="{{asset('images/processing.gif')}}"/>
                        </form>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="contact_address wow fadeInRight">
                        <h3>Address</h3>
                        <div class="address_group">
                            <p>{{$address}}</p>
                            <p>{{$postal_address}}</p>
                            <p>Phone: {{$phones}}</p>
                            <p>Email: <a href="mailto:{{$email}}">{{$email}} </a></p>
                        </div>
                        <div class="address_group">
                            <ul class="footer_social">
                                <li><a data-toggle="tooltip" data-placement="top" title="Facebook" class="soc_tooltip" href="{{$facebook}}"><i class="fa fa-facebook"></i></a></li>
                                <li><a data-toggle="tooltip" data-placement="top" title="Twitter" class="soc_tooltip"  href="{{$twitter}}"><i class="fa fa-twitter"></i></a></li>
                                <li><a data-toggle="tooltip" data-placement="top" title="Google+" class="soc_tooltip"  href="{{$googlePlus}}"><i class="fa fa-google-plus"></i></a></li>
                                <li><a data-toggle="tooltip" data-placement="top" title="Linkedin" class="soc_tooltip"  href="{{$linked}}"><i class="fa fa-linkedin"></i></a></li>
                                <li><a data-toggle="tooltip" data-placement="top" title="Youtube" class="soc_tooltip"  href="{{$youtube}}"><i class="fa fa-youtube"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--=========== END CONTACT SECTION ================-->
@endsection

@section('script')
    <script type='text/javascript'>

        //hide loading gif
        $('#yourLoadingImage').hide();

        $('#form1').on('submit', function (e) {
            //FORM IS VALID
            //stop default form submission so you can do stuff
            e.preventDefault();
            $('#sm').hide();
            $('#yourLoadingImage').show();
            //do stuff
            //submit form if desired
            this.submit();
        });
    </script>
@endsection
