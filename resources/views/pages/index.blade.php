<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <!--===============================================
   Template Design By WpFreeware Team.
   Author URI : http://www.wpfreeware.com/
   ====================================================-->

    <!-- Basic Page Needs
    ================================================== -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Salt Institute : Home</title>

    <!-- Mobile Specific Metas
    ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/png" href="{{asset('favicon.ico')}}"/>

    <!-- CSS
    ================================================== -->
    <!-- Bootstrap css file-->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Font awesome css file-->
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
    <!-- Superslide css file-->
    <link rel="stylesheet" href="{{ asset('css/superslides.css') }}">
    <!-- Slick slider css file -->
    <link href="{{ asset('css/slick.css') }}" rel="stylesheet">
    <!-- Slick slider theme css file -->
    <link href="{{ asset('css/slick-theme.css') }}" rel="stylesheet">
    <!-- Circle counter cdn css file -->
    <link rel='stylesheet prefetch' href='https://cdn.rawgit.com/pguso/jquery-plugin-circliful/master/css/jquery.circliful.css'>
    <!-- smooth animate css file -->
    <link rel="stylesheet" href="{{ asset('css/animate.css') }}">
    <!-- preloader -->
{{--    <link rel="stylesheet" href="{{ asset('css/queryLoader.css') }}" type="text/css" />--}}
    <!-- gallery slider css -->
    <link type="text/css" media="all" rel="stylesheet" href="{{ asset('css/jquery.tosrus.all.css') }}" />
    <!-- Default Theme css file -->
    <link id="switcher" href="{{ asset('css/themes/default-theme.css') }}" rel="stylesheet">
    <!-- Main structure css file -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Google fonts -->
    <link href='http://fonts.googleapis.com/css?family=Merriweather' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Varela' rel='stylesheet' type='text/css'>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
</head>
<body>
    <!-- SCROLL TOP BUTTON -->
    <a class="scrollToTop" href="#"></a>
    <!-- END SCROLL TOP BUTTON -->

    <!--=========== BEGIN HEADER SECTION ================-->
    <header id="header">
        <!-- BEGIN MENU -->
        <div class="menu_area">
            <nav class="navbar navbar-default navbar-fixed-top" role="navigation">  <div class="container">
                    <div class="navbar-header">
                        <!-- FOR MOBILE VIEW COLLAPSED BUTTON -->
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <!-- LOGO -->
                        <!-- TEXT BASED LOGO -->
                        <a class="navbar-brand" href="{{ route('home') }}"><span style="padding-bottom: 10px;"><img src= "{{ asset('images/salt_logo.png') }}" alt="salt_logo" height="55px" width="130px"></span></a>


                    </div>
                    <div id="navbar" class="navbar-collapse collapse">
                        <ul id="top-menu" class="nav navbar-nav navbar-right main-nav">
                            <li class="active"><a href="{{ route('home') }}">Home</a></li>
                            <li><a href="{{ route('about') }}">About Us</a></li>
                            <li class="dropdown">
                                <a href="{{ route('programme') }}" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Programmes<span class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    @foreach($programmes as $programme)
                                        <li><a href="{{ route('single.programme', ['id' => $programme->id]) }}">{{$programme->programme_name}}</a></li>
                                    @endforeach
                                </ul>
                            </li>

                            <li class="dropdown">
                                <a  class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Our Staff<span class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="{{route('board')}}">Board</a></li>
                                    <li><a href="{{route('team')}}">Faculty</a></li>
                                </ul>
                            </li>
                            <li><a href="{{route('contact')}}">Contact</a></li>
                        </ul>
                    </div><!--/.nav-collapse -->
                </div>
            </nav>
        </div>
        <!-- END MENU -->
    </header>
    <!--=========== END HEADER SECTION ================-->

    <!--=========== BEGIN SLIDER SECTION ================-->
    <section id="slider">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="slider_area">
                    <!-- Start super slider -->
                    <div id="slides">
                        <ul class="slides-container">
                            @foreach($quotes as $quote)
                                <li>
                                    <img src="{{$quote->slider_image_name}}" alt="img" height="400px" width="1080px">
                                    <div class="slider_caption center">
                                        <h6 style="width: 100%; background: rgba(10,51,80, 0.5); border-radius: 8px; border:3px solid #fff; padding-top:20px; padding-bottom: 20px; padding-left: 20px; text-align: left; line-height: 30px;">{{$quote->quote}}
                                            @if($quote->speaker !== '' && $quote->speaker !== null)
                                            <br/><br/>~ {{$quote->speaker}} @if($quote->speaker_position !== '' && $quote->speaker_position !== null)({{$quote->speaker_position}}) @endif</h6>
                                            @else
                                            </h6>
                                            @endif
                                        <a class="slider_btn" href="{{ route('about') }}">Know More</a>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                        <nav class="slides-navigation">
                            <a href="#" class="next"></a>
                            <a href="#" class="prev"></a>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--=========== END SLIDER SECTION ================-->
    <div class="clr"></div>

    <hr/>

    <section id="aboutUs">
        <div class="container">
            <h2 class="titile wow fadeInLeft">Upcoming Events:</h2>
            <div class="row">
                <!-- Start about us area -->
                <div class="col-lg-10 col-md-12 col-sm-12">
                    <div class="aboutus_area wow fadeInLeft">
                        <div>
                            <img src="{{asset(Storage::url('general/saltAdSlider1.jpg'))}}"
                                 style="width: 100%; height: 700px; margin-bottom: 20px;">

                            <a href="{{route('course.registration')}}" class="btn btn-success" style="margin-right: 50px;">Click here to fill the form online</a>

                            <a href="{{asset('downloads/ExecutiveCourseRegistrationForm.pdf')}}" download="SALT Executive Course Registration Form">Click here to download the registration form.</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <hr/>

    <!--=========== BEGIN FOOTER SECTION ================-->
    @include('partials._footer')
    <!--=========== END FOOTER SECTION ================-->

    <script src="{{ asset('js/html5shiv.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('js/respond.min.js')}}" type="text/javascript"></script>
    <!-- Javascript Files
    ================================================== -->

    <!-- initialize jQuery Library -->
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <!-- Preloader js file -->
{{--    <script src="{{ asset('js/queryloader2.min.js') }}" type="text/javascript"></script>--}}
    <!-- For smooth animatin  -->
    <script src="{{ asset('js/wow.min.js') }}"></script>
    <!-- Bootstrap js -->
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <!-- slick slider -->
    <script src="{{ asset('js/slick.min.js') }}"></script>
    <!-- superslides slider -->
    <script src="{{ asset('js/jquery.easing.1.3.js') }}"></script>
    <script src="{{ asset('js/jquery.animate-enhanced.min.js') }}"></script>
    <script src="{{ asset('js/jquery.superslides.min.js') }}" type="text/javascript" charset="utf-8"></script>
    <!-- for circle counter -->
    <script src='{{ asset('js/jquery.circliful.min.js') }}'></script>
    <!-- Gallery slider -->
    <script type="text/javascript" language="javascript" src="{{ asset('js/jquery.tosrus.min.all.js') }}"></script>

    <!-- Custom js-->
    <script src="{{ asset('js/custom.js') }}"></script>
    <!--===============================================
    Template Design By WpFreeware Team.
    Author URI : http://www.wpfreeware.com/
    ====================================================-->
</body>
</html>
