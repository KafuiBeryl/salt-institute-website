@extends('layouts.layout')

@section('header')
    <ul id="top-menu" class="nav navbar-nav navbar-right main-nav">
        <li ><a href="{{ route('home') }}">Home</a></li>
        <li><a href="{{ route('about') }}">About Us</a></li>
        <li class="dropdown">
            <a href="{{ route('programme') }}" class="dropdown-toggle" role="button" aria-expanded="false">Programmes<span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
                @foreach($programmes as $programme)
                    <li><a href="{{ route('single.programme', ['id' => $programme->id]) }}">{{$programme->programme_name}}</a></li>
                @endforeach
            </ul>
        </li>

        <li class="dropdown active">
            <a  class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Our Staff<span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
                <li><a href="{{route('board')}}">Board</a></li>
                <li><a href="{{route('team')}}">Faculty</a></li>
            </ul>
        </li>
        <li><a href="{{route('contact')}}">Contact</a></li>
    </ul>
@endsection

@section('banner')
    <div id="DIV_1">
        <div id="DIV_2">
            <div id="DIV_3">
                <div id="DIV_4">
                    <div id="DIV_5">
                        <h1 id="H1_6">
                            Board of Trustees
                        </h1>
                    </div>
                </div>
                <div id="DIV_7">
                </div>
            </div>
        </div>
    </div>
@endsection


@section('body')
    <!--=========== BEGIN ABOUT TRUSTEES SECTION ================-->
    <section id="aboutUs" style="min-height: 300px">
        @foreach($trustees as $trustee)
            <div id="centerpole" class="panel">
                <div style="margin-bottom: 3em;" id="DIV_23" class="container center">
                    <div id="DIV_24">
                        <div id="DIV_25">
                            <div id="DIV_26">

                                <div id="DIV_27">
                                    <h2 id="H2_28">
                                        {{$trustee->title}} {{$trustee->first_name}} {{$trustee->last_name}} ({{$trustee->country}}) - {{$trustee->position}}
                                        <img src="{{$trustee->country_image_name}}" height="30px" width="60px">
                                    </h2>
                                </div>

                                <div id="DIV_29">
                                    <span id="SPAN_30"><span id="SPAN_31"></span></span>
                                    <h4 id="H4_32">
                                        {{$trustee->position}}
                                    </h4> <span id="SPAN_33"><span id="SPAN_34"></span></span>
                                </div>

                                <div id="DIV_35">
                                    <figure id="FIGURE_36">
                                        <div id="DIV_37">
                                            <img style="float: left; margin-right: 1.5em; margin-bottom: 1em;" src="{{$trustee->image_name}}" width="400" height="370" alt="Barrister" id="IMG_38" />
                                        </div>
                                        <figcaption id="FIGCAPTION_39" style="color: #0e71b8">
                                            {{$trustee->title}} {{$trustee->first_name}} {{$trustee->last_name}}
                                        </figcaption>
                                    </figure>
                                </div>

                                <div id="DIV_40">
                                    <div id="DIV_41">
                                        <p>{{$trustee->description}}</p>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </section>
    <!--=========== END ABOUT TRUSTEES SECTION ================-->
@endsection
