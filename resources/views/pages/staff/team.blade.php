@extends('layouts.layout')

@section('header')
    <ul id="top-menu" class="nav navbar-nav navbar-right main-nav">
        <li ><a href="{{ route('home') }}">Home</a></li>
        <li><a href="{{ route('about') }}">About Us</a></li>
        <li class="dropdown">
            <a href="{{ route('programme') }}" class="dropdown-toggle" role="button" aria-expanded="false">Programmes<span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
                @foreach($programmes as $programme)
                    <li><a href="{{ route('single.programme', ['id' => $programme->id]) }}">{{$programme->programme_name}}</a></li>
                @endforeach
            </ul>
        </li>

        <li class="dropdown active">
            <a  class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Our Staff<span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
                <li><a href="{{route('board')}}">Board</a></li>
                <li><a href="{{route('team')}}">Faculty</a></li>
            </ul>
        </li>
        <li><a href="{{route('contact')}}">Contact</a></li>
    </ul>
@endsection

@section('banner')
    <section id="imgBanner_about"></section>
@endsection


@section('body')
    <!--=========== BEGIN FACULTY SECTION ================-->
    <section id="aboutUs">
        <div class="wow fadeInUp" style="padding-left: 35%">
            <img  style="width: 50%;" src="{{asset('images/Quote_1.jpg')}}">
        </div>
        <hr/>
        <div class="container" style="padding-left: 5%">
            <div class="row">
                <!-- Start about us area -->
                <div class="col-lg-6 col-md-6 col-sm-6" style="padding-top: 90px">
                    <h2 class="titile wow fadeInLeft">The Core Management Team</h2>
                    <div class="aboutus_area wow fadeInLeft">
                        <p style="font-size: 16px">
                        <ul style="list-style-type:disc; margin-left:20px; font-size: 16px" >
                            @foreach($cores as $core)
                                <li>{{$core->title}} {{$core->first_name}} {{$core->last_name}}, {{$core->position}}</li>
                            @endforeach
                        </ul>
                        </p>
                    </div>
                </div>


                <div class="col-lg-6 col-md-6 col-sm-6">

                    <h2 class="titile wow fadeInLeft">The Faculty</h2>

                    <div class="aboutus_area wow fadeInLeft">
                        <p style="font-size: 16px">
                        <ul style="list-style-type:disc; margin-left:20px; font-size: 16px" >
                            @foreach($faculties as $faculty)
                                <li>
                                    @if($faculty->title !== '')
                                        {{$faculty->title}}
                                    @endif
                                        {{$faculty->title}} {{$faculty->first_name}} {{$faculty->last_name}}
                                @if($faculty->suffix !== '')
                                        , {{$faculty->suffix}}
                                @endif
                                </li>
                            @endforeach
                        </ul>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div>
        </div>
        <hr/>

        <div class="container" style="padding-left: 4%">
            <div class="col-lg-6 col-md-6 col-sm-6">
                <div class="aboutus_area wow fadeInLeft">
                    <h2 class="titile wow fadeInLeft">AUTHORISATION</h2>
                    <p style="font-size: 16px">{{$authorization}}</p>
                </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-6">
                <div class="aboutus_area wow fadeInLeft">
                    <h2 class="titile wow fadeInLeft">AFFILIATION</h2>
                    <p style="font-size: 16px">{{$affiliation}}</p>
                </div>
            </div>
        </div>
    </section>
    <!--=========== END FACULTY SECTION ================-->
@endsection
