@extends('layouts.programmes')

@section('header')
    <ul id="top-menu" class="nav navbar-nav navbar-right main-nav">
        <li ><a href="{{ route('home') }}">Home</a></li>
        <li><a href="{{ route('about') }}">About Us</a></li>
        <li class="dropdown active">
            <a href="{{ route('programme') }}" class="dropdown-toggle" role="button" aria-expanded="false">Programmes<span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
                @foreach($programmes as $programme)
                    <li><a href="{{ route('single.programme', ['id' => $programme->id]) }}">{{$programme->programme_name}}</a></li>
                @endforeach
            </ul>
        </li>

        <li class="dropdown">
            <a  class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Our Staff<span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
                <li><a href="{{route('board')}}">Board</a></li>
                <li><a href="{{route('team')}}">Faculty</a></li>
            </ul>
        </li>
        <li><a href="{{route('contact')}}">Contact</a></li>
    </ul>
@endsection

@section('banner')
    <section id="imgBanner_Programme"></section>
@endsection


@section('body')
    <!--=========== BEGIN ABOUT PROGRAMMES SECTION ================-->
    <section id="aboutUs" style="min-height: 300px">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-lg-offset-3 col-md-offset-3 col-sm-offset-3">
                    <div class="title_area">
                        <h2 class="title_two wow fadeInUp">Programmes:</h2>
                        <span></span>
                        <div class="wow fadeInUp" style="margin-top: 20px">
                            <p style="text-align: left;font-size: 16px"> {{$programme_pretext}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--=========== END ABOUT PROGRAMMES SECTION ================-->
@endsection
