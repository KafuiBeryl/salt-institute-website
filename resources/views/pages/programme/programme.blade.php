@extends('layouts.programmes')

@section('header')
    <ul id="top-menu" class="nav navbar-nav navbar-right main-nav">
        <li ><a href="{{ route('home') }}">Home</a></li>
        <li><a href="{{ route('about') }}">About Us</a></li>
        <li class="dropdown active">
            <a href="{{ route('programme') }}" class="dropdown-toggle" role="button" aria-expanded="false">Programmes<span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
                @foreach($programmes as $programme)
                    <li><a href="{{ route('single.programme', ['id' => $programme->id]) }}">{{$programme->programme_name}}</a></li>
                @endforeach
            </ul>
        </li>

        <li class="dropdown">
            <a  class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Our Staff<span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
                <li><a href="{{route('board')}}">Board</a></li>
                <li><a href="{{route('team')}}">Faculty</a></li>
            </ul>
        </li>
        <li><a href="{{route('contact')}}">Contact</a></li>
    </ul>
@endsection

@section('banner')
    <section id="slider" style="position: relative; overflow: hidden; width: 100%; height: 474px;">

        <img src="{{$actual_programme->banner_image}}" data-focus-left=".79" data-focus-top=".12" data-focus-right=".30" data-focus-bottom=".66">

    </section>
@endsection


@section('body')
    <!--=========== BEGIN PROGRAMMME SECTION ================-->
    <section id="aboutUs" style="min-height: 600px; width:relative ; position: center">



        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="title_area text-center">
                        <h2 class="title_two wow fadeInUp">{{$actual_programme->programme_name}}</h2>
                        <span></span>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-lg-offset-3 col-md-offset-3 col-sm-offset-3">
                    <div class="wow fadeInUp" style="margin-top: 20px">
                        <p style="font-size: 16px">{{$actual_programme->programme_blurb}}</p>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-lg-offset-3 col-md-offset-3 col-sm-offset-3">
                    <div class="newsfeed_area wow fadeInUp" style="margin-top: 20px">
                        <p style="font-size: 20px; color: #009fe3"> <strong> MAIN SUBJECTS INCLUDE:</strong> </p>
                        <p style="font-size: 16px">
                        <ul style="list-style-type:disc; margin-left:20px; font-size: 16px" >
                            @foreach($courses as $course)
                                <li>{{$course->course_name}}</li>
                            @endforeach
                        </ul>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--=========== END PROGRAMMME SECTION ================-->
@endsection
