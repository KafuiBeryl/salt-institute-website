@extends('layouts.layout')

@section('header')
    <ul id="top-menu" class="nav navbar-nav navbar-right main-nav">
        <li ><a href="{{ route('home') }}">Home</a></li>
        <li class="active"><a href="{{ route('about') }}">About Us</a></li>
        <li class="dropdown">
            <a href="{{ route('programme') }}" class="dropdown-toggle" role="button" aria-expanded="false">Programmes<span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
                @foreach($programmes as $programme)
                    <li><a href="{{ route('single.programme', ['id' => $programme->id]) }}">{{$programme->programme_name}}</a></li>
                @endforeach
            </ul>
        </li>

        <li class="dropdown">
            <a  class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Our Staff<span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
                <li><a href="{{route('board')}}">Board</a></li>
                <li><a href="{{route('team')}}">Faculty</a></li>
            </ul>
        </li>
        <li><a href="{{route('contact')}}">Contact</a></li>
    </ul>
@endsection

@section('banner')
    <section id="imgBanner_about"></section>
@endsection


@section('body')
    <!--=========== BEGIN ABOUT US SECTION ================-->
    <section id="aboutUs">
        <div class="container">
            <h2 class="titile wow fadeInLeft">Origin</h2>
            <div class="row">
                <!-- Start about us area -->
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="aboutus_area wow fadeInLeft">
                        <p>
                            {{$origin}}
                        </p>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="newsfeed_area wow fadeInRight">
                        <p> <strong> {{$strategy_pretext}}</strong> </p>
                        <p>
                        <ul style="list-style-type:disc; margin-left:20px;" >
                            @foreach($strategies as $strategy)
                                <li>{{$strategy->content}}</li>
                            @endforeach
                        </ul>
                        </p>
                    </div>
                </div>
            </div>
            <hr/>
            <div>
                <img src="{{asset(Storage::url('general/saltAdSlider1.jpg'))}}"
                     style="width: 100%; height: 700px; margin-bottom: 20px;">

                <a href="{{route('course.registration')}}" class="btn btn-success" style="margin-right: 50px;">Click here to fill the form online</a>

                <a href="{{asset('downloads/ExecutiveCourseRegistrationForm.pdf')}}" download="SALT Executive Course Registration Form">Click here to download the registration form.</a>
            </div>
            <div>
                <hr/>

                <h2 class="titile wow fadeInLeft">Vision:</h2>
                <p>{{$vision}}</p>
                <h2 class="titile wow fadeInLeft">Mission: </h2>
                <p>{{$mission}}</p>
                <hr/>

            </div>
        </div>
    </section>
    <!--=========== END ABOUT US SECTION ================-->

    <!--=========== BEGIN WHY US SECTION ================-->
    <section id="whyUs">
        <!-- Start why us top -->
        <div class="row">
            <div class="col-lg-12 col-sm-12">
                <div class="whyus_top">
                    <div class="container">
                        <!-- Why us top titile -->
                        <div class="row">
                            <div class="col-lg-12 col-md-12">
                                <div class="title_area">
                                    <h2 class="title_two">Our Values</h2>
                                    <span></span>
                                    <p>{{$values_pretext}}</p>
                                </div>
                            </div>
                        </div>
                        <!-- End Why us top titile -->
                        <!-- Start Why us top content  -->
                        <div class="row" style="margin-bottom: 25px">
                            @foreach($values as $value)
                                <div class="col-lg-3 col-md-3 col-sm-6">
                                    <div class="single_whyus_top wow fadeInUp">
                                        <div class="whyus_icon">
                                            <span class="{{$value->icon_name}}"></span>
                                        </div>
                                        <h3>{{$value->name}}</h3>
                                        <p>{{$value->content}}</p>
                                    </div>
                                </div>
                            @endforeach
                        <!-- End Why us top content  -->
                    </div>
                </div>
            </div>
            <!-- End why us top -->
        </div>
        </div>
    </section>
    <!--=========== END WHY US SECTION ================-->
@endsection
