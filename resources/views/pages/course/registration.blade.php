@extends('layouts.layout')

@section('extracss')
    <link href="{{ asset('css/main.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('header')
    <ul id="top-menu" class="nav navbar-nav navbar-right main-nav">
        <li ><a href="{{ route('home') }}">Home</a></li>
        <li ><a href="{{ route('about') }}">About Us</a></li>
        <li class="dropdown">
            <a href="{{ route('programme') }}" class="dropdown-toggle" role="button" aria-expanded="false">Programmes<span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
                @foreach($programmes as $programme)
                    <li><a href="{{ route('single.programme', ['id' => $programme->id]) }}">{{$programme->programme_name}}</a></li>
                @endforeach
            </ul>
        </li>

        <li class="dropdown">
            <a  class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Our Staff<span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
                <li><a href="{{route('board')}}">Board</a></li>
                <li><a href="{{route('team')}}">Faculty</a></li>
            </ul>
        </li>
        <li><a href="{{route('contact')}}">Contact</a></li>
    </ul>
@endsection

@section('banner')
    <section id="imgBanner_about"></section>
@endsection

@section('body')
    <br>
    <section id="register_form">
        <div class="content-title">
            <h4 class="text-center">SALT INSTITUTE EXECUTIVE CERTIFICATE COURSE IN <br> INTERNATIONAL RELATIONS & TRANSFORMATIONAL LEADERSHIP February 05 – May 21, 2019</h4><br>
            <h3 class="content-title">REGISTRATION FORM</h3>
        </div>
        <div class="content-title-underline"></div>

        <div class="container">
            @if (\Session::has('message'))
                <div class="alert alert-success">
                    <p>{{ \Session::get('message') }}</p>
                </div><br />
            @endif
            <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-10">
                    <div class="panel panel-info">
                        <div class="panel-heading">Please fill out the form below. All fields are required.</div>
                        <div class="panel-body">
                            <form class="form" role="form" method="post" action="{{route('course.registration.submit')}}">
                                {{ csrf_field() }}
                                <div class="form-group{{ $errors->has('tin_number') ? ' has-error' : '' }} col-md-12 FormFill_area">
                                    <label>TIN No.</label>
                                    <input type="text" name="tin_number" value="{{old('tin_number')}}" class="form-control" required autofocus>
                                    @if ($errors->has('tin_number'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('tin_number') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} col-md-12 FormFill_area">
                                    <label>Name</label>
                                    <input type="text" name="name" value="{{old('name')}}" class="form-control">
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="form-group FormFill_area">
                                    <label for="" class="col-md-2 control-label">Gender</label>
                                    <div class="col-md-10">
                                        <label class="checkbox-inline Radio_Checkbox_space"><input name="gender" type="radio" value="Male">Male</label>
                                        <label class="checkbox-inline"><input name="gender" type="radio" value="Female" checked>Female</label>
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }} col-md-12 FormFill_area">
                                    <label>Title</label>
                                    <input type="text" name="title" value="{{old('title')}}" class="form-control" required autofocus>
                                    @if ($errors->has('title'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="form-group{{ $errors->has('organization') ? ' has-error' : '' }} col-md-12 FormFill_area">
                                    <label>Organization</label>
                                    <input type="text" name="organization" value="{{old('organization')}}" class="form-control" required autofocus>
                                    @if ($errors->has('organization'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('organization') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="form-group{{ $errors->has('position') ? ' has-error' : '' }} col-md-12 FormFill_area">
                                    <label>Designation/Position</label>
                                    <input type="text" name="position" value="{{old('position')}}" class="form-control" required autofocus>
                                    @if ($errors->has('position'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('position') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="form-group{{ $errors->has('duties') ? ' has-error' : '' }} col-md-12 FormFill_area">
                                    <label>Current duties</label>
                                    <input type="text" name="duties" value="{{old('duties')}}" class="form-control" required autofocus>
                                    @if ($errors->has('duties'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('duties') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }} col-md-12 FormFill_area">
                                    <label>Address</label>
                                    <input type="text" name="address" value="{{old('address')}}" class="form-control" required autofocus>
                                    @if ($errors->has('address'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }} col-md-6 FormFill_area">
                                    <label>Mobile/Telephone</label>
                                    <input type="text" name="phone" value="{{old('phone')}}" class="form-control" required autofocus>
                                    @if ($errors->has('phone'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} col-md-6 FormFill_area">
                                    <label>Email</label>
                                    <input type="email" name="email" value="{{old('email')}}" class="form-control" required autofocus>
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="form-group FormFill_area">
                                    <label for="" class="col-md-2 control-label">Course</label>
                                    <div class="col-md-10">
                                        <label class="checkbox-inline Radio_Checkbox_space"><input name="course" value="Transformational Leadership" type="radio" checked>1. Transformational Leadership </label>
                                        <label class="checkbox-inline "><input name="course" value="International Relations" type="radio">2. International Relations </label>
                                    </div>
                                </div>

                                <div class="col-md-12 FormFill_area">
                                    <div class="form-group{{ $errors->has('preferred_schedule') ? ' has-error' : '' }}">
                                        <label for="">Preferred Course Time schedule:</label>
                                        <select class="form-control" name="preferred_schedule">
                                            <option value="">-- Select Here --</option>
                                            <option value="Weekdays (Monday - Friday) 5:30pm - 8:00pm daily">Weekdays (Monday - Friday) 5:30pm - 8:00pm daily </option>
                                            <option value="Weekends (Saturdays) 8:30am - 2:30pm daily">Weekends (Saturdays) 8:30am - 2:30pm daily</option>
                                        </select>
                                        @if ($errors->has('preferred_schedule'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('preferred_schedule') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-12 FormFill_area">
                                    <div class="form-group{{ $errors->has('qualification') ? ' has-error' : '' }}">
                                        <label for="">Level of education:</label>
                                        <select class="form-control" name="qualification" >
                                            <option value="">-- Select Here --</option>
                                            <option value="HND">HND</option>
                                            <option value="BSc / BA">BSc / BA</option>
                                            <option value="MA / MSc">MA / MSc</option>
                                            <option value="PhD / DBA">PhD / DBA</option>
                                        </select>
                                        @if ($errors->has('qualification'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('qualification') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-12 fieldset_area">
                                    <fieldset>
                                        <legend>Learning goals | Expectations | Personal input </legend>
                                        <div class="inputBox">
                                            <div class="inputText">1. What is your three main objectives for this course? </div>
                                            <input type="text" name="goal1" value="{{old('goal1')}}" class="input" placeholder="Goal 1">
                                            @if ($errors->has('goal1'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('goal1') }}</strong>
                                                </span>
                                            @endif
                                            <input type="text" name="goal2" value="{{old('goal2')}}" class="input" placeholder="Goal 2">
                                            @if ($errors->has('goal2'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('goal2') }}</strong>
                                                </span>
                                            @endif
                                            <input type="text" name="goal3" value="{{old('goal3')}}" class="input" placeholder="Goal 3">
                                            @if ($errors->has('goal3'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('goal3') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        <div class="inputBox">
                                            <div class="inputText">2. List three of your top expectations </div>
                                            <input type="text" name="expectations1" value="{{old('expectations1')}}" class="input" placeholder="Expectation 1">
                                            @if ($errors->has('expectations1'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('expectations1') }}</strong>
                                                </span>
                                            @endif
                                            <input type="text" name="expectations2" value="{{old('expectations2')}}" class="input" placeholder="Expectation 2">
                                            @if ($errors->has('expectations2'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('expectations2') }}</strong>
                                                </span>
                                            @endif
                                            <input type="text" name="expectations3" value="{{old('expectations3')}}" class="input" placeholder="Expectation 3">
                                            @if ($errors->has('expectations3'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('expectations3') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        <div class="inputBox I_will_part">
                                            <div class="inputText">3. List three areas where you can contribute to the success of the course: </div>
                                            <input type="text" name="contribute1" value="{{old('contribute1')}}" class="input" placeholder="I will...">
                                            @if ($errors->has('contribute1'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('contribute1') }}</strong>
                                                </span>
                                            @endif
                                            <input type="text" name="contribute2" value="{{old('contribute2')}}" class="input" placeholder="I will...">
                                            @if ($errors->has('contribute2'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('contribute2') }}</strong>
                                                </span>
                                            @endif
                                            <input type="text" name="contribute3" value="{{old('contribute3')}}" class="input" placeholder="I will...">
                                            @if ($errors->has('contribute3'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('contribute3') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </fieldset>
                                </div>

                                <div class="form-group text-center">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn  btn-lg btn-primary">
                                            Submit
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-1"></div>
        </div>
        </div>
    </section>

    <div class="container">
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10">
                <div class="Payment_Instructions">
                    <h3>Payment Instructions</h3>
                    <p>Full course fee payable in two installments is Gh¢ 2,900 plus a non-refundable fee of Ghs150</p>
                    <p>Pay course fees to Account Name: SUNDOULOS ADVANCED LEADERSHIP TRAINING (SALT) INSTITUTE </p>
                    <p>UBA Ghana Limited Account No: 012 117 451 03503</p>
                    <p>Mobile Money Number: 055 248 0164/ 050 312 2676</p>
                </div>
                <div class="Please_note">
                    <h4>Please note that we only accept payments that also cover the transfer fees.</h4>
                    <p>Reference: Candidate's full name and course selected.</p>
                    <p>Any financial matters should be addressed to: <a href="mailto:falabo@saltinstituteghana.org">falabo@saltinstituteghana.org</a></p>
                    <p>Submit application form by email to <a href="mailto:info@saltinstituteghana.org">info@saltinstituteghana.org</a> by 03 February 2019</p>
                </div>
            </div>
            <div class="col-md-1"></div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(".input").focus(function(){
            $(this).parent().addClass("focus")
        }).blur(function(){
            if($(this).val() === ''){
                $(this).parent().addClass("focus")
            }
        })
    </script>
@endsection