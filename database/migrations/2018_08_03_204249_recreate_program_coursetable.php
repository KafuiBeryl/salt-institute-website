<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RecreateProgramCoursetable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('programme_course', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('programme_id');
            $table->foreign('programme_id')
                ->references('id')
                ->on('programmes');
            $table->string('course_code',50);
            $table->foreign('course_code')
                ->references('course_code')
                ->on('courses');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('programme_course');
    }
}
