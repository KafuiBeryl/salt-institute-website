<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrusteeBoardTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trustee_board', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 10)->nullable();
            $table->string('first_name',50);
            $table->string('last_name',50);
            $table->string('country_image_name', 30);
            $table->string('country', 30);
            $table->string('position', 50);
            $table->string('image_url', 100)->nullable();
            $table->text('description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trustee_board');
    }
}
