<?php

use App\Models\TrusteeBoard;
use Illuminate\Database\Seeder;

class TrusteeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TrusteeBoard::truncate();

        TrusteeBoard::create([
            'title' => 'Barrister',
            'first_name' => 'Emeka',
            'last_name' => 'Nwankpa',
            'country_image_name' => 'nigeria',
            'country' => 'Nigeria',
            'position' => 'Chairman',
            'image_name' => 'emeka_nwankpa',
            'description' => 'Emeka Nwankpa is a lawyer by profession. He is the Chairman of Intercessors for Africa and has been involved in the ministry of intercession for over four decades. He is also the Chairman of Sundoulos African Leadership Training (SALT) Programme.
Emeka is an Adviser to the International Christian Chamber of Commerce as well as Chairman, Board of Trustees, Pastors/ Ministers Prayer Network. He chairs the following institutions: a) the Nigerian Consumer Protection Council of the Federal Ministry of Industry, Trade and Investment; b) Academic Board of the Africa Pastors and Ministers Prayer Network; c) Board of Trustees, Intercessors for Nigeria; and d) Board of Trustees, Intercessors for Africa (IFA).
Prior to these, he served as the Pro-Chancellor and Chairman, Governing Council of Abia State University, and Member, Committee of Pro-Chancellors of Nigerian Universities. He has authored several books, including: “Redeeming the Land – Interceding for Nations”; “The Lord is a Man of War – A Manual for Spiritual Warfare”; “Idolatry – Principles, Problems and Panacea”. 
Emeka received his law degree from the University of Ile-Ife and Nigerian Law School, Lagos. He is an Alumnus of the Advanced Leadership Training Course of the Haggai Institute, Singapore.
He is married to Bade Nwankpa, a retired Pharmacist, and they have three adult children and several grandchildren. He speaks Hausa, Igbo and Yoruba fluently, in addition to English.
'
        ]);

        TrusteeBoard::create([
            'title' => 'Pastor Dr.',
            'first_name' => 'James',
            'last_name' => 'Obeng',
            'country_image_name' => 'ghana',
            'country' => 'Ghana',
            'position' => 'Member',
            'image_name' => 'james_obeng',
            'description' => 'Pastor Obeng is the General Overseer of Evangel Church International and the National Co-Ordinator of Network of Intercessors of Ghana. He is also the Chairman of Evangel Church Biblical Training Institute (EBTI). 
He is involved in pastoring and in the intercessory movement in Ghana. His ministry has taken him to countries in Africa, Asia, Europe, North America, and the Far East.
Prior to taking up full time ministry, he worked as a Senior Public Relations Officer at the Ghana Highway Authority. He is the author of several books, including “The Fire of the Holy Spirit” and “Raising Champions in Times of Crisis”. 
Pastor Obeng received his bachelor’s degree in Communications and Biblical Studies at Evangel College in Springfield, Missouri; Master’s and Doctorate degrees in Theology at the Andersonville Theological Seminary, Camilla, Georgia, USA. He is an alumnus of the Haggai Advanced Leadership Institute, Singapore. 
He is married to Pastor Jemima Obeng and the marriage is blessed with four children and six grandchildren.
'

        ]);
        TrusteeBoard::create([
            'title' => 'Dr.',
            'first_name' => 'Fatima',
            'last_name' => 'Alabo',
            'country_image_name' => 'ghana',
            'country' => 'Ghana',
            'position' => 'Member',
            'image_name' => 'fatima_alabo',
            'description' => 'Fatima is the Co-Founder and Director of Royal Standard Learning Centre in Accra and a member of the Board of Trustees of the Intercessors for Africa (IFA).
Over the years, Fatima has pursued research work in history, international relations and modern languages. She was a Senior Lecturer in History and Russian Language at the Hassan II University in Casablanca and served as the Director of External Relations with the Moroccan Post and Telecommunications. She was the pioneer Head of the French Department of the Central University College, Ghana’s premier private university and Acting Dean of the Faculty of Arts and Social Studies of the University. Fatima was actively involved in social work in Italy, Angola, India and Russia while on the diplomatic circuit with her husband, Ambassador Dr. Kodzo Alabo. In Russia, she was the President of the Group of African Ambassadors’ Wives (GAAW). In that role, Fatima spearheaded major projects, including securing grants and making donations to orphanages in Moscow as well as Ebola victims in Sierra Leone, Liberia and Guinea. Outside academia and diplomacy, Fatima is an ordained Minister of the Gospel. She was instrumental in planting of churches in Angola, India and Russia. In Russia, she pastored the Christian House Chapel (CHC) in down- town Moscow and created a vibrant worship experience for a growing international congregation. Fatima possesses a Master of Arts degree in History and PhD in International Relations, both from the Kiev State University, Ukraine. Fatima is married with two adult children and a grandson. She is fluent in English, French, Arabic, Russian and Italian.'

        ]);
        TrusteeBoard::create([
            'title' => 'Dr.',
            'first_name' => 'Steve',
            'last_name' => 'Ogan',
            'country_image_name' => 'nigeria',
            'country' => 'Nigeria',
            'position' => 'Member',
            'image_name' => 'steve_ogan',
            'description' => 'Steve is the President of High Calling Outreach, a Literature Evangelism Ministry, which distributes Christian Literature and mobilizes support for battered women, abused widows and the destitute.
He is a member of the Board of Trustees of several organizations, including the Intercessors for Africa, Abia State University of Agriculture Chapel, the Rainbow Book Club which won the prestigious Port Harcourt World Book Project of UNESCO. 
Steve has authored more than 150 books which have been published in Nigeria, East Africa, the United Kingdom, India and Malaysia. He was given a Bestsellers Award by the Uzima Publishers in Nairobi, Kenya for two of his books: “How to Beat Your Husband” and “How to Beat Your Wife.”
Steve holds a Bachelor of Arts Degree in History from the University of Port Harcourt, as well as Master of Arts and Doctorate degrees in Legal History from the University of Ibadan. He is married to Atim Sofiri Ogan, and they have five biological sons and many adopted children and grandchildren
.'

        ]);
        TrusteeBoard::create([
            'title' => 'Dr.',
            'first_name' => 'Kodzo',
            'last_name' => 'Alabo',
            'country_image_name' => 'ghana',
            'country' => 'Ghana',
            'position' => 'Member',
            'image_name' => 'kodzo_alabo',
            'description' => 'Kodzo is a member of the Board of Trustees of the Intercessors for Africa (IFA). He also leads an independent consulting practice in Diplomacy and International Economic Relations.
He is a career diplomat with strong track record spanning over three decades with the Ghana Ministry of Foreign Affairs and Regional Integration (MFARI). He served in Italy, Angola, India and Russia and became the Ambassador Extraordinary and Plenipotentiary to the Russian Federation with concurrent accreditation to Armenia, Azerbaijan, Belarus, Kazakhstan, Moldova, Mongolia, and Ukraine.
Prior to being appointed Ambassador, Kodzo served in various senior positions, including being the Director of the Africa and Regional Integration Bureau of MFARI, making him the Government’s Chief Adviser on African, African Union and ECOWAS affairs; he was also a member of the Ghana delegation during the negotiation of the United Nations Arms Trade Treaty. He was instrumental in major milestones in bilateral and multilateral business negotiations with other African countries.
Kodzo was educated at the University of Ghana, Legon and Kiev State University’s Faculty of International Relations and International Law in Kiev, Ukraine. He possesses master’s and PhD degrees in International Economic Relations. He also holds post-graduate diplomas in Management and Public Administration from the Ghana Institute of Management and Public Administration (GIMPA), and International Relations from Nairobi University in Kenya.
Besides English, he is fluent in Russian and French and has working knowledge of Italian and Portuguese. An ordained Minister, Kodzo is married to Dr. Fatima Alabo and they are blessed with two adult children and a grand- son.'

        ]);
        TrusteeBoard::create([
            'title' => 'Dr.',
            'first_name' => 'James',
            'last_name' => 'Magara',
            'country_image_name' => 'uganda',
            'country' => 'Uganda',
            'position' => 'Member',
            'image_name' => 'james_magara',
            'description' => 'James is a cherished voice on leadership in Uganda and beyond. His passion to see Africa’s immense human capital put to use is evident in his book, “Positioning Africa in the 21st Century: The Pivotal Role of Leadership and Think Tanks”.
James has been actively involved in raising the standard of leadership across Africa by raising, developing, mentoring and equipping other leaders. He is a facilitator and curriculum developer for servant leadership and business training programmes including Sundoulos African Leaders Training (SALT) Programme, Africa Kingdom Business Forum (AKBF), and Institute for National Transformation (INT) where he is the International Deputy Director General. 
Since 2007 he has been the chairman of Uganda Youth Forum and currently the board chairman of Uganda Heart Institute.
He holds Doctorate degree in Strategic Leadership from Regent University, Virginia Beach, USA; Master of Arts in Organizational Leadership and Management from Uganda Christian University, Mukono; Master of Science in Dental Prosthetics from University of Bristol (UK) and Bachelor of Dental Surgery from Makerere University in Kampala, Uganda. He is an alumnus of the Haggai Advanced Leadership Institute, Singapore.
He is married to Lorna. They have brought up four adult children, Timothy, Joseph, David, and Jemima. They live in Kampala, Uganda.
'

        ]);
        TrusteeBoard::create([
            'title' => 'Dr.',
            'first_name' => 'Reuben',
            'last_name' => 'Coffie',
            'country_image_name' => 'ghana',
            'country' => 'Ghana',
            'position' => 'Member/Secretary',
            'image_name' => 'reuben_coffie',
            'description' => 'Until joining SALT Institute, Reuben was the CEO of yesaAfrica. He has served in senior leadership roles in international development across East, Southern and Western Africa. These include, principally, work with, Care International, Chemonics International, SNV Netherlands Development Organization, DAI Europe and The Palladium Group. 
He has previously led the strategic and organizational development of the University of Health and Allied Sciences (UHAS) and Centre for Democratic Development (CDD), Ghana. He also served as Senior Technical Advisor for SNV Netherlands Development Organization in East and Southern Africa covering Ethiopia, Kenya, South Sudan, Tanzania, Rwanda, Mozambique and Zimbabwe.
Reuben received his bachelor’s and master’s degrees in Economics at Peoples Friendship University of Russia, MBA certificate at Oklahoma Christian University/Peoples Friendship University of Russia and a Doctorate degree in Business Administration (Business Leadership & Management) at Walden University in Minneapolis.
Reuben is fluent in English and Russian and has working knowledge of French. He is married to Diana Coffie and they are blessed with three children.
'
        ]);

    }
}
