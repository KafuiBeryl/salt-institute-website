<?php

use App\Models\SchoolValue;
use Illuminate\Database\Seeder;

class ValuesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SchoolValue::truncate();

        SchoolValue::create([
            'name' => 'Scripturally Driven',
            'content' => 'We strive to be a model organization, honouring God in our corporate lifestyle, achieving a creditable reputation in the marketplace based on Biblical principles.',
            'icon_name' => 'fa fa-book'
        ]);

        SchoolValue::create([
            'name' => 'Passionate',
            'content' => 'Passionate We love the things we do and strongly believe in transformational leadership inspired by scriptural foundations',
            'icon_name' => 'fa fa-heart'
        ]);

        SchoolValue::create([
            'name' => 'Innovative',
            'content' => 'We constantly improve ourselves and find unique ways to deliver our training and learning programmes.',
            'icon_name' => 'fa fa-flask'
        ]);

        SchoolValue::create([
            'name' => 'Responsive',
            'content' => 'We proactively engage our learners and other stakeholders and remain accountable to them.',
            'icon_name' => 'fa fa-support'
        ]);

        SchoolValue::create([
            'name' => 'Individual Growth',
            'content' => 'We create an environment that fosters systematic growth of individuals to become forces of positive influence while in the institute and when they enter the market place.',
            'icon_name' => 'fa fa-line-chart'
        ]);

        SchoolValue::create([
            'name' => 'Teamwork',
            'content' => 'We create an environment that produces unity of purpose, synergistic relationships and optimum results.',
            'icon_name' => 'fa fa-group'
        ]);
    }
}
