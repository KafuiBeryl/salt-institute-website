<?php

use App\Models\CoreManagement;
use Illuminate\Database\Seeder;

class CoreTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CoreManagement::create([
            'title' => 'Dr.',
            'first_name' => 'Kodzo',
            'last_name' => 'Alabo',
            'position' => 'Rector'
        ]);

        CoreManagement::create([
            'title' => 'Dr.',
            'first_name' => 'Fatima',
            'last_name' => 'Alabo',
            'position' => 'Director of Finance and Administration'
        ]);

        CoreManagement::create([
            'title' => 'Dr.',
            'first_name' => 'Reuben',
            'last_name' => 'Coffie',
            'position' => 'Registrar'
        ]);
    }
}
