<?php

use App\Models\Course;
use Illuminate\Database\Seeder;

class CourseTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        Course::truncate();

        Course::create([
            /*--------------------TRAN711------------------*/
            'course_name' => 'The foundations of effective leadership',
            'course_code' => 'TRAN711'
        ]);

        Course::create([
            'course_name' => 'Servant leadership and the leader without a title',
            'course_code' => 'TRAN712'
        ]);

        Course::create([
            'course_name' => 'Influence, Persuasion, and Negotiation',
            'course_code' => 'TRAN713'
        ]);

        Course::create([
            'course_name' => 'Difficult Conversations as a Leader',
            'course_code' => 'TRAN714'
        ]);

        Course::create([
            'course_name' => 'Reflections on the importance of learning as a leader',
            'course_code' => 'TRAN715'
        ]);

        Course::create([
            'course_name' => 'Reputation capital and footprints of a leader',
            'course_code' => 'TRAN716'
        ]);

        Course::create([
            'course_name' => 'Planning for next step in your professional career',
            'course_code' => 'TRAN717'
        ]);

        Course::create([
            /*-----------------INT711--------------------*/
            'course_name' => 'The history and dynamics of International Relations',
            'course_code' => 'INT711'
        ]);

        Course::create([
            'course_name' => 'International Economic Relations',
            'course_code' => 'INT712'
        ]);

        Course::create([
            'course_name' => 'International Trade',
            'course_code' => 'INT713'
        ]);

        Course::create([
            'course_name' => 'International Organization',
            'course_code' => 'INT714'
        ]);

        Course::create([
            /*-------------------------TRAN continuation--------------------*/
            'course_name' => 'Understanding leadership and rules of synergy',
            'course_code' => 'TRAN718'
        ]);

        Course::create([
            'course_name' => ' Leadership versus management',
            'course_code' => 'TRAN719'
        ]);

        Course::create([
            'course_name' => 'Ethics of leadership and the rules of engagement',
            'course_code' => 'TRAN7120'
        ]);

        Course::create([
            'course_name' => 'Biblical models of leadership and case studies',
            'course_code' => 'TRAN721'
        ]);

        Course::create([
            'course_name' => 'Jesus, the hallmark of servant leadership – case study',
            'course_code' => 'TRAN722'
        ]);

        Course::create([
            'course_name' => 'Communication management',
            'course_code' => 'TRAN723'
        ]);

        Course::create([
            'course_name' => 'Leading and Managing teams',
            'course_code' => 'TRAN724'
        ]);

        Course::create([
            'course_name' => 'Effective attitudes in leadership ',
            'course_code' => 'TRAN725'
        ]);

        Course::create([
            'course_name' => 'Organizational development and behaviour',
            'course_code' => 'TRAN726'
        ]);

        Course::create([
            'course_name' => 'Manpower and succession planning in leadership',
            'course_code' => 'TRAN727'
        ]);

        Course::create([
            'course_name' => ' Research methods and applications in leadership',
            'course_code' => 'TRAN728'
        ]);

        Course::create([
            /*-------------------------INT continued------------------------*/
            'course_name' => 'The theories and practice of international relations',
            'course_code' => 'INT715'
        ]);

        Course::create([
            'course_name' => ' Government and political institutions',
            'course_code' => 'INT716'
        ]);

        Course::create([
            'course_name' => 'Leadership and administration',
            'course_code' => 'INT717'
        ]);

        Course::create([
            'course_name' => 'Comparative political systems',
            'course_code' => 'INT718'
        ]);

        Course::create([
            'course_name' => 'Political communication management',
            'course_code' => 'INT719'
        ]);

        Course::create([
            'course_name' => 'International trade systems',
            'course_code' => 'INT720'
        ]);

        Course::create([
            'course_name' => 'Political history and political science',
            'course_code' => 'INT721'
        ]);

        Course::create([
            'course_name' => 'Political economy and global market issues',
            'course_code' => 'INT722'
        ]);

        Course::create([
            'course_name' => 'International law and politics',
            'course_code' => 'INT723'
        ]);

        Course::create([
            'course_name' => 'International organizations and politics',
            'course_code' => 'INT724'
        ]);

        Course::create([
            'course_name' => 'Research and reporting',
            'course_code' => 'INT725'
        ]);

        Course::create([
            /*-------------------------TRAN111----------------------------*/
            'course_name' => 'Principles of transformational leadership ',
            'course_code' => 'TRAN111'
        ]);

        Course::create([
            'course_name' => 'Survey of great leaders',
            'course_code' => 'TRAN112'
        ]);

        Course::create([
            'course_name' => 'Biblical models of leadership',
            'course_code' => 'TRAN113'
        ]);

        Course::create([
            'course_name' => 'Ethics of leadership',
            'course_code' => 'TRAN114'
        ]);

        Course::create([
            'course_name' => 'Innovation leadership',
            'course_code' => 'TRAN115'
        ]);

        Course::create([
            'course_name' => 'Communication and influence',
            'course_code' => 'TRAN116'
        ]);

        Course::create([
            'course_name' => 'Strategic planning',
            'course_code' => 'TRAN117'
        ]);

        Course::create([
            'course_name' => 'Organizational dynamics',
            'course_code' => 'TRAN118'
        ]);

        Course::create([
            'course_name' => 'Managerial decision-making',
            'course_code' => 'TRAN119'
        ]);

        Course::create([
            'course_name' => 'Managing and leading teams effectively',
            'course_code' => 'TRAN120'
        ]);

        Course::create([
            'course_name' => 'Research methods and application',
            'course_code' => 'TRAN121'
        ]);
        Course::create([
            /*-------------------------INT111------------------------*/
            'course_name' => 'Introduction to international relations',
            'course_code' => 'INT111'
        ]);

        Course::create([
            'course_name' => 'Diplomatic protocol and etiquette',
            'course_code' => 'INT112'
        ]);

        Course::create([
            'course_name' => 'Foreign policy and diplomacy of African countries',
            'course_code' => 'INT113'
        ]);

        Course::create([
            'course_name' => 'Case study of Ghana’s foreign policy and diplomacy',
            'course_code' => 'INT114'
        ]);

        Course::create([
            'course_name' => 'Foreign policy of the USA and western European countries',
            'course_code' => 'INT115'
        ]);

        Course::create([
            'course_name' => ' Foreign policy of Russia and eastern European countries',
            'course_code' => 'INT116'
        ]);

        Course::create([
            'course_name' => 'The history of international relations',
            'course_code' => 'INT117'
        ]);

        Course::create([
            'course_name' => 'Introduction to international law',
            'course_code' => 'INT118'
        ]);

        Course::create([
            'course_name' => 'Political economy, governance and political structures',
            'course_code' => 'INT119'
        ]);

        Course::create([
            'course_name' => 'Human rights in world politics',
            'course_code' => 'INT120'
        ]);

        Course::create([
            'course_name' => 'Introduction to international organizations',
            'course_code' => 'INT121'
        ]);

        Course::create([
            'course_name' => 'Introduction to international economic relations',
            'course_code' => 'INT122'
        ]);

        Course::create([
            'course_name' => 'Theory of international relations',
            'course_code' => 'INT1123'
        ]);

        Course::create([
            /*--------------------TRAN511----------------------*/
            'course_name' => 'Leadership and Management Skills for Non-Managers',
            'course_code' => 'TRAN511'
        ]);

        Course::create([
            'course_name' => 'Leadership Skills and Techniques',
            'course_code' => 'TRAN512'
        ]);

        Course::create([
            'course_name' => 'Breakthrough Problem Solving',
            'course_code' => 'TRAN513'
        ]);

        Course::create([
            'course_name' => 'Foundation role model in servant leadership',
            'course_code' => 'TRAN514'
        ]);

        Course::create([
            'course_name' => 'Engaging Leadership',
            'course_code' => 'TRAN515'
        ]);

        Course::create([
            'course_name' => 'From Tactical to Strategic Thinking',
            'course_code' => 'TRAN516'
        ]);

        Course::create([
            'course_name' => 'Advanced Leadership Skills and Techniques',
            'course_code' => 'TRAN517'
        ]);

        Course::create([
            'course_name' => 'Navigating Change in Turbulent Times',
            'course_code' => 'TRAN518'
        ]);

        Course::create([
            /*---------------INT511------------------*/
            'course_name' => 'Peace, conflict, and negotiations',
            'course_code' => 'INT511'
        ]);

        Course::create([
            'course_name' => 'Human rights',
            'course_code' => 'INT512'
        ]);

        Course::create([
            'course_name' => 'Trade, resources, and international business',
            'course_code' => 'INT513'
        ]);

        Course::create([
            'course_name' => 'Research methods and reporting',
            'course_code' => 'INT514'
        ]);

        Course::create([
            /*------MOVE DOWN---LEAD712----------*/
            'course_name' => 'International Law',
            'course_code' => 'LEAD712'
        ]);

        Course::create([
            'course_name' => 'International Economics',
            'course_code' => 'LEAD713'
        ]);

        Course::create([
            /*-------------MOVE DOWN---LEAD112-----------*/
            'course_name' => 'Principles of servant leadership',
            'course_code' => 'LEAD112'
        ]);

        Course::create([/*-----------MOVE DOWN---LEAD711-------------*/
            'course_name' => 'Critical thinking and problem solving and decision making',
            'course_code' => 'LEAD711'
        ]);

        Course::create([
            /*---------------MOVE DOWN----LEAD511-----------------------*/
            'course_name' => 'Communicating Strategically',
            'course_code' => 'LEAD511'
        ]);

        Course::create([
            'course_name' => 'Creativity and Innovation ',
            'course_code' => 'LEAD512'
        ]);

        Course::create([
            'course_name' => 'Critical Thinking for Problem Solving',
            'course_code' => 'LEAD513'
        ]);

        Course::create([/*-------------------------MOVE DOWN-----LEAD514-----------*/
            'course_name' => 'Negotiation Skills',
            'course_code' => 'LEAD514'
        ]);
    }
}
