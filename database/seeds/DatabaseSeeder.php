<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call([
             SchoolInfoSeeder::class,
             UsersTableSeeder::class,
             TrusteeTableSeeder::class,
             ValuesTableSeeder::class,
             ProgrammeTableSeeder::class,
             QuoteTableSeeder::class,
             CourseTableSeeder::class,
             FacultyTableSeeder::class,
             AddressTableSeeder::class
         ]);
    }
}
