<?php

use App\Models\ProgrammeCourse;
use Illuminate\Database\Seeder;

class ProgrammeCourseTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        ProgrammeCourse::create([
            'programme_id' => '1',
            'course_code' => 'TRAN711'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '1',
            'course_code' => 'TRAN712'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '1',
            'course_code' => 'TRAN713'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '1',
            'course_code' => 'TRAN714'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '1',
            'course_code' => 'TRAN715'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '1',
            'course_code' => 'TRAN716'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '1',
            'course_code' => 'TRAN717'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '3',
            'course_code' => 'TRAN718'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '3',
            'course_code' => 'TRAN719'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '3',
            'course_code' => 'TRAN720'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '3',
            'course_code' => 'TRAN721'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '3',
            'course_code' => 'TRAN722'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '3',
            'course_code' => 'TRAN723'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '3',
            'course_code' => 'TRAN724'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '3',
            'course_code' => 'TRAN725'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '3',
            'course_code' => 'TRAN726'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '3',
            'course_code' => 'TRAN727'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '3',
            'course_code' => 'TRAN728'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '2',
            'course_code' => 'INT711'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '2',
            'course_code' => 'INT712'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '2',
            'course_code' => 'INT713'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '2',
            'course_code' => 'INT714'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '4',
            'course_code' => 'INT715'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '4',
            'course_code' => 'INT716'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '4',
            'course_code' => 'INT717'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '4',
            'course_code' => 'INT718'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '4',
            'course_code' => 'INT719'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '4',
            'course_code' => 'INT720'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '4',
            'course_code' => 'INT721'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '4',
            'course_code' => 'INT722'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '4',
            'course_code' => 'INT723'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '4',
            'course_code' => 'INT724'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '4',
            'course_code' => 'INT725'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '5',
            'course_code' => 'TRAN111'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '5',
            'course_code' => 'TRAN112'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '5',
            'course_code' => 'TRAN113'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '5',
            'course_code' => 'TRAN114'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '5',
            'course_code' => 'TRAN115'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '5',
            'course_code' => 'TRAN116'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '5',
            'course_code' => 'TRAN117'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '5',
            'course_code' => 'TRAN118'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '5',
            'course_code' => 'TRAN119'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '5',
            'course_code' => 'TRAN120'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '5',
            'course_code' => 'TRAN121'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '6',
            'course_code' => 'INT111'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '6',
            'course_code' => 'INT112'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '6',
            'course_code' => 'INT113'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '6',
            'course_code' => 'INT114'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '6',
            'course_code' => 'INT115'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '6',
            'course_code' => 'INT116'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '6',
            'course_code' => 'INT117'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '6',
            'course_code' => 'INT118'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '6',
            'course_code' => 'INT119'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '6',
            'course_code' => 'INT120'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '6',
            'course_code' => 'INT121'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '6',
            'course_code' => 'INT122'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '6',
            'course_code' => 'INT123'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '7',
            'course_code' => 'TRAN511'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '7',
            'course_code' => 'TRAN512'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '7',
            'course_code' => 'TRAN513'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '7',
            'course_code' => 'TRAN514'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '7',
            'course_code' => 'TRAN515'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '7',
            'course_code' => 'TRAN516'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '7',
            'course_code' => 'TRAN517'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '7',
            'course_code' => 'TRAN518'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '8',
            'course_code' => 'INT511'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '8',
            'course_code' => 'INT512'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '8',
            'course_code' => 'INT513'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '8',
            'course_code' => 'INT514'
        ]);
        /*---=======They have more than one programme=======---*/

        //For first and eighth programmes
        ProgrammeCourse::create([
            'programme_id' => '2',
            'course_code' => 'LEAD712'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '8',
            'course_code' => 'LEAD712'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '2',
            'course_code' => 'LEAD713'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '8',
            'course_code' => 'LEAD713'
        ]);
        /*------------//-------------------*/


        //For third and fifth programmes
        ProgrammeCourse::create([
            'programme_id' => '3',
            'course_code' => 'LEAD112'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '5',
            'course_code' => 'LEAD112'
        ]);
        /*---------------//---------------*/


        //For third and forth programmes
        ProgrammeCourse::create([
            'programme_id' => '3',
            'course_code' => 'LEAD711'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '4',
            'course_code' => 'LEAD711'
        ]);
        /*------------//-----------------*/


        //For seventh and eighth programmes
        ProgrammeCourse::create([
            'programme_id' => '7',
            'course_code' => 'LEAD511'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '7',
            'course_code' => 'LEAD512'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '7',
            'course_code' => 'LEAD513'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '7',
            'course_code' => 'LEAD514'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '8',
            'course_code' => 'LEAD511'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '8',
            'course_code' => 'LEAD512'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '8',
            'course_code' => 'LEAD513'
        ]);

        ProgrammeCourse::create([
            'programme_id' => '8',
            'course_code' => 'LEAD514'
        ]);
        /*---------------//---------------*/
    }
}
