<?php

use App\Models\AddressInfo;
use Illuminate\Database\Seeder;

class AddressTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AddressInfo::truncate();

        AddressInfo::create([
            'name' => 'address',
            'content' => 'Sundoulos Advanced Leadership Training (SALT) Institute 64 Motorway Extension, Dzorwulu George Walker Bush Highway'
        ]);

        AddressInfo::create([
            'name' => 'postal_address',
            'content' => 'P. O. Box DC 682 Accra, Ghana'
        ]);

        AddressInfo::create([
            'name' => 'telephone_1',
            'content' => '+233 26 320 6645'
        ]);

        AddressInfo::create([
            'name' => 'telephone_2',
            'content' => '+233 20 815 5544'
        ]);

        AddressInfo::create([
            'name' => 'email_address',
            'content' => 'info@saltinstituteghana.org'
        ]);

        AddressInfo::create([
            'name' => 'website',
            'content' => 'www.saltinstituteghana.org'
        ]);

        AddressInfo::create([
            'name' => 'contact_title',
            'content' => 'WE ARE SALT INSTITUTE'
        ]);

        AddressInfo::create([
            'name' => 'contact_pretext',
            'content' => 'CENTRE OF EXCELLENCE IN TRANSFORMATIONAL LEADERSHIP AND INTERNATIONAL RELATIONS'
        ]);

        AddressInfo::create([
            'name' => 'facebook_link',
            'content' => ''
        ]);

        AddressInfo::create([
            'name' => 'twitter_link',
            'content' => ''
        ]);

        AddressInfo::create([
            'name' => 'google_plus_link',
            'content' => ''
        ]);

        AddressInfo::create([
            'name' => 'linked_in_link',
            'content' => ''
        ]);

        AddressInfo::create([
            'name' => 'youtube_link',
            'content' => ''
        ]);
    }
}
