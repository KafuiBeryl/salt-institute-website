<?php

use App\Models\SchoolInfo;
use Illuminate\Database\Seeder;

class SchoolInfoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SchoolInfo::truncate();


        SchoolInfo::create([
            'name' => 'origin',
            'content' => 'Sundoulos Advanced Leadership Training (SALT) Institute was founded in 2003 but its genesis dates to 1997. During that year’s edition of the annual continental prayer conference of the Intercessors for Africa (IFA), held in Addis Ababa, Ethiopia, God revealed to the intercessors that they faced “challenges in Africa that prayer alone by itself could not solve.” It was further explained that African nations did not have good, proper foundations (the foundations not having been laid on the scriptures); secondly, the nations did not have servant leaders in their governments; thirdly, leaders in the nations did not know how to apply biblical principles to leadership. To tackle these challenges, IFA was directed to train and equip the leaders who will bring about the desired transformation in the land.

SALT Institute is therefore a response to that call. The institute is committed to raising, training, and developing servant leaders for business and public life in Africa as well as the global marketplace.'
        ]);

        SchoolInfo::create([
            'name' => 'strategy_pretext',
            'content' => 'The strategy is to train and build leaders who, will be able to, inter alia'
        ]);

        SchoolInfo::create([
            'name' => 'values_pretext',
            'content' => 'The Power and influence of the Holy Spirit encapsulates the values of SALT Institute.'
        ]);

        SchoolInfo::create([
            'name' => 'vision',
            'content' => 'A world class knowledge and learning centre dedicated to the development of transformational leaders for Africa and the global community.'
        ]);

        SchoolInfo::create([
            'name' => 'mission',
            'content' => 'To maximize the human capital dividend of Africa through the delivery of time tested scripture-based knowledge, practices and mindset.'
        ]);

        SchoolInfo::create([
            'name' => 'faculty_pretext',
            'content' => 'Driven by a mindset for transformation in Africa, our Faculty bring a strong blend of real life leadership experiences and sterling academic practice to bear on our core competencies: integrity, stewardship and leadership.'
        ]);

        SchoolInfo::create([
            'name' => 'authorization',
            'content' => 'SALT Institute’s operations are authorised by the National Accreditation Board (Ghana)'
        ]);

        SchoolInfo::create([
            'name' => 'affiliation',
            'content' => 'SALT Institute is affiliated to the University of Port Harcourt, Nigeria'
        ]);
    }
}
