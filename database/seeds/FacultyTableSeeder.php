<?php

use App\Models\Faculty;
use Illuminate\Database\Seeder;

class FacultyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Faculty::truncate();

        Faculty::create([
            'title' => '',
            'first_name' => 'Abednego',
            'last_name' => 'Offori_Addo',
            'suffix' => 'MD'
        ]);

        Faculty::create([
            'title' => 'Amb.',
            'first_name' => 'Kodzo',
            'last_name' => 'Alabo',
            'suffix' => 'PhD'
        ]);

        Faculty::create([
            'title' => 'Amb.',
            'first_name' => 'Mercy',
            'last_name' => 'Debrah-Karikari',
            'suffix' => 'MA'
        ]);

        Faculty::create([
            'title' => 'Amb.',
            'first_name' => 'Novisi Aku',
            'last_name' => 'Abiadoo',
            'suffix' => 'MA'
        ]);

        Faculty::create([
            'title' => 'Amd',
            'first_name' => 'Elizabeth',
            'last_name' => 'Adjei',
            'suffix' => 'MA'
        ]);

        Faculty::create([
            'title' => '',
            'first_name' => 'Chidinma',
            'last_name' => 'Braye-Yankee',
            'suffix' => 'MA'
        ]);

        Faculty::create([
            'title' => '',
            'first_name' => 'Emeka',
            'last_name' => 'Nwankpa',
            'suffix' => 'MA'
        ]);

        Faculty::create([
            'title' => '',
            'first_name' => 'Ernest Nana',
            'last_name' => 'Adjei',
            'suffix' => 'MA'
        ]);

        Faculty::create([
            'title' => '',
            'first_name' => 'Fatima',
            'last_name' => 'Alabo',
            'suffix' => 'PhD'
        ]);

        Faculty::create([
            'title' => '',
            'first_name' => 'George',
            'last_name' => 'Asumadu',
            'suffix' => 'PhD'
        ]);

        Faculty::create([
            'title' => '',
            'first_name' => 'Gordon',
            'last_name' => 'Abeka-Nkrumah',
            'suffix' => 'PhD'
        ]);

        Faculty::create([
            'title' => '',
            'first_name' => 'James',
            'last_name' => 'Obeng',
            'suffix' => 'PhD'
        ]);

        Faculty::create([
            'title' => '',
            'first_name' => 'Larry',
            'last_name' => 'Attipoe',
            'suffix' => 'MA'
        ]);

        Faculty::create([
            'title' => '',
            'first_name' => 'Mawuli',
            'last_name' => 'Coffie',
            'suffix' => 'DBA'
        ]);
    }
}
