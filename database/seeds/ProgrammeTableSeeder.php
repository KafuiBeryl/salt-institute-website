<?php

use App\Models\Programme;
use Illuminate\Database\Seeder;

class ProgrammeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        Programme::truncate();

        Programme::create([
            'programme_name' => 'Post Graduate Certificate in Transformational Leadership',
            'programme_blurb' => 'It is a generalist leadership programme designed for middle-level management who require to improve their understanding of the modern trends in leadership that produce tangible and lasting change in organizations.

The Programme consists of courses that give insight into emerging leadership frameworks, analytical and problem-solving skills and techniques.
At the heart of each course is the foundation of integrity, accountable leadership and self-management.',
            'programme_image_name' => 'Course_01'
        ]);

        Programme::create([
            'programme_name' => 'Post Graduate Certificate in International Relations',
            'programme_blurb' => 'It is a generalist leadership programme designed for middle-level management who require to improve their understanding of the modern trends in leadership that produce tangible and lasting change in organizations.

This is a generalist programme for equipping leaders with understanding and skills to operate in the global environment. ',
            'programme_image_name' => 'Course_02'
        ]);

        Programme::create([
            'programme_name' => 'Post Graduate Diploma in Transformational Leadership',
            'programme_blurb' => 'Designed for middle-top level management, the programme is aimed at acquainting participants with an in-depth understanding of leadership and principles underpinning effective leadership. It is built on the time-tested foundation of how to achieve success without bending ethical principles.',
            'programme_image_name' => 'Course_03'
        ]);

        Programme::create([
            'programme_name' => 'Post Graduate Diploma in International Relations',
            'programme_blurb' => 'Effective leaders require good understanding and skills to operate in the global environment.

The programme equips participants with basic knowledge in the history and dynamics of international relations, international economic relations, international trade, international law and international organizations.',
            'programme_image_name' => 'Course_04'
        ]);

        Programme::create([
            'programme_name' => 'Bachelor of Arts in Transformational Leadership',
            'programme_blurb' => 'Designed to offer advanced training in transformational leadership, the programme has a strong organizational component while providing a broader perspective on leadership. The study of topics such as principles of servant leadership, decision making, conflict resolution, knowledge management, ethics and reputation capital equips students with varied nuances of leadership that can be applied across multiple disciplines.

The courses emphasise leadership from a Christian perspective and give students strong foundational knowledge of leadership theories, concepts and principles and equips them with skills to immediately apply the knowledge in their places of work and influence.

The degree programme builds on the modern concepts of leadership and how effective leaders create lasting foundation for sustainable organizations.',
            'programme_image_name' => 'Course_05'
        ]);

        Programme::create([
            'programme_name' => 'Bachelor of Arts in International Relations',
            'programme_blurb' => 'The programme aims at equipping learners with knowledge and analytical skills to enable them address seemingly intractable global problems and the political nature of responses to them and to consider whether there are global responsibilities that result therefrom. The courses advance critical knowledge about mitigating conflict and promoting cooperation within and across states. In addition, they provide insight into relationships between international structures, processes and political institutions, including states, international organizations as well as social movements and international non-governmental organizations (NGOs).

The programme focuses on the broad areas of foreign policy, international political economy, global governance and international law, contemporary global politics and economics. In the end, learners develop solid intellectual grounding in the key debates, historical events, and political institutions in a global world.',
            'programme_image_name' => 'Course_06'
        ]);

        Programme::create([
            'programme_name' => 'Master of Arts in Transformational Leadership',
            'programme_blurb' => 'Gain the knowledge and experience you need to successfully prepare for new leadership roles, increase emotional intelligence and engagement capabilities, handle complex problems, and lead change.

This programme is designed for individuals at all levels who want to exercise stronger leadership capabilities.',
            'programme_image_name' => 'Course_07'
        ]);

        Programme::create([
            'programme_name' => 'Master of Arts in International Relations',
            'programme_blurb' => 'SALT Institute’s Master of Arts in International Relations and Diplomacy is an academic programme that prepares students not only for careers in the foreign service, in intergovernmental organizations, or international businesses, but also in such fields as journalism, non-governmental watchdogs or NGOs, foreign policy think tanks and academic research.

Drawing from multiple fields, including finance, law, history, philosophy, and theology, this programme is decidedly interdisciplinary. The rigorous nature of the programme challenges students to develop an in-depth understanding of international affairs and international diplomacy. It is designed for high achievers seeking top level roles in their career. 
Students will analyse current issues in the field within specific historical and cultural contexts, producing research-based writing, participating in debates, and gaining practical training in negotiation and mediation of conflicts.',
            'programme_image_name' => 'Course_08'
        ]);
    }
}
