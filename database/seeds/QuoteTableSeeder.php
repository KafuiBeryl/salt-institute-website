<?php

use App\Models\Quote;
use Illuminate\Database\Seeder;

class QuoteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Quote::truncate();

        Quote::create([
            'quote' => 'Effective leadership leads to sustainable transformation of everything around us. 
Transformational leaders are cultivated as individuals who can think and act with 
integrity and mindset that does not accept the status quo',
            'speaker' => 'Dr. Kodzo Alabo',
            'speaker_position' => 'Rector, SALT Institute',
            'slider_image_name' => 'slider1'
        ]);

        Quote::create([
            'quote' => 'A new kind of leadership training institute to churn out tomorrow’s global leaders today.',
            'speaker' => '',
            'speaker_position' => '',
            'slider_image_name' => 'slider2'
        ]);

        Quote::create([
            'quote' => 'Create of excellence in Transformational Leadership and International Relations.',
            'speaker' => '',
            'speaker_position' => '',
            'slider_image_name' => 'slider3'
        ]);
    }
}
