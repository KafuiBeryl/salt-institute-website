<?php

use App\Models\Strategy;
use Illuminate\Database\Seeder;

class StrategyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Strategy::create([
            'content' => 'Lay new foundations in leadership based on godly principles for African peoples and the nations'
        ]);

        Strategy::create([
            'content' => 'Mobilize human resources and build coalitions from available human resources with a view to providing the best possible governance'
        ]);

        Strategy::create([
            'content' => 'Translate God’s plan for their nations into reality by planned implementation;'
        ]);

        Strategy::create([
            'content' => 'Understand what will transform African nations economically, socially, and politically; '
        ]);

        Strategy::create([
            'content' => 'Interpret national crises correctly, work out solutions and implement recommendations accurately; '
        ]);

        Strategy::create([
            'content' => 'Handle the intrigues of governance and leadership without losing personal integrity; '
        ]);

        Strategy::create([
            'content' => 'Become channels through which righteousness and justice can flow to impact people and politics.'
        ]);
    }
}
