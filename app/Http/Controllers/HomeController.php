<?php

namespace App\Http\Controllers;

use App\Mail\Contact;
use App\Models\AddressInfo;
use App\Models\Course;
use App\Models\Faculty;
use App\Models\Programme;
use App\Models\ProgrammeCourse;
use App\Models\Quote;
use App\Models\SchoolInfo;
use App\Models\SchoolValue;
use App\Models\Strategy;
use App\Models\TrusteeBoard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Alert;
use Mockery\Exception;

class HomeController extends Controller
{

    public function index(){
        $programs = Programme::select('id','programme_name')->where('active', 1)->get();
//        print_r($programs);
//        die();
        $quotes = Quote::orderBy('id', 'desc')->get();
        $quotes->each(function ($item){
            $item['slider_image_name'] = asset(Storage::url('general/'.$item['slider_image_name']));
        });
        $address = AddressInfo::where('name', 'address')->first()->content;
        $postalAddress = AddressInfo::where('name', 'postal_address')->first()->content;
        $firstPhone = AddressInfo::where('name', 'telephone_1')->first()->content;
        $secondPhone = AddressInfo::where('name', 'telephone_2')->first()->content;
        $phoneNumber = $firstPhone;
        if ($secondPhone !== ''){
            $phoneNumber = $firstPhone.' | '.$secondPhone;
        }
        $emailAddress = AddressInfo::where('name', 'email_address')->first()->content;
        $facebook = AddressInfo::where('name', 'facebook_link')->first()->content;
        $twitter = AddressInfo::where('name', 'twitter_link')->first()->content;
        $instagram = AddressInfo::where('name', 'instagram_link')->first()->content;
        $linked_in = AddressInfo::where('name', 'linked_in_link')->first()->content;
//        $youtube = AddressInfo::where('name', 'youtube_link')->first()->content;
        return view('pages.index', [
            'programmes' => $programs,
            'quotes' => $quotes,
            'address' => $address,
            'postal_address' => $postalAddress,
            'phones' => $phoneNumber,
            'email' => $emailAddress,
            'facebook' => $facebook,
            'twitter' => $twitter,
            'instagram' => $instagram,
            'linked' => $linked_in
//            'youtube' => $youtube,
        ]);
    }

    public function about(){
        $programs = Programme::where('active', 1)->get();
        $origin = SchoolInfo::where('name', 'origin')->first()->content;
        $strategy_pretext = SchoolInfo::where('name', 'strategy_pretext')->first()->content;
        $values_pretext = SchoolInfo::where('name', 'values_pretext')->first()->content;
        $vision = SchoolInfo::where('name', 'vision')->first()->content;
        $mission = SchoolInfo::where('name', 'mission')->first()->content;
        $strategies = Strategy::all();
        $values = SchoolValue::all();
//        print_r($values);
//        die();
        //footer shit
        $address = AddressInfo::where('name', 'address')->first()->content;
        $postalAddress = AddressInfo::where('name', 'postal_address')->first()->content;
        $firstPhone = AddressInfo::where('name', 'telephone_1')->first()->content;
        $secondPhone = AddressInfo::where('name', 'telephone_2')->first()->content;
        $phoneNumber = $firstPhone;
        if ($secondPhone !== ''){
            $phoneNumber = $firstPhone.' | '.$secondPhone;
        }
        $emailAddress = AddressInfo::where('name', 'email_address')->first()->content;
        $facebook = AddressInfo::where('name', 'facebook_link')->first()->content;
        $twitter = AddressInfo::where('name', 'twitter_link')->first()->content;
        $instagram = AddressInfo::where('name', 'instagram_link')->first()->content;
        $linked_in = AddressInfo::where('name', 'linked_in_link')->first()->content;
//        $youtube = AddressInfo::where('name', 'youtube_link')->first()->content;
        return view('pages.about', [
            'programmes' => $programs,
            'origin' => $origin,
            'strategy_pretext' => $strategy_pretext,
            'values_pretext' => $values_pretext,
            'vision' => $vision,
            'mission' => $mission,
            'strategies' => $strategies,
            'values' => $values,
            'address' => $address,
            'postal_address' => $postalAddress,
            'phones' => $phoneNumber,
            'email' => $emailAddress,
            'facebook' => $facebook,
            'twitter' => $twitter,
            'instagram' => $instagram,
            'linked' => $linked_in
//            'youtube' => $youtube,
        ]);
    }

    public function programLanding(){
        $programs = Programme::where('active', 1)->get();
        $programs->each(function ($item){
            $item['programme_image_name'] = asset(Storage::url('programmes/'.$item['programme_image_name'].'.jpg'));
        });
        $programme_pretext = SchoolInfo::where('name', 'programme_pretext')->first()->content;

        //footer shit
        $address = AddressInfo::where('name', 'address')->first()->content;
        $postalAddress = AddressInfo::where('name', 'postal_address')->first()->content;
        $firstPhone = AddressInfo::where('name', 'telephone_1')->first()->content;
        $secondPhone = AddressInfo::where('name', 'telephone_2')->first()->content;
        $phoneNumber = $firstPhone;
        if ($secondPhone !== ''){
            $phoneNumber = $firstPhone.' | '.$secondPhone;
        }
        $emailAddress = AddressInfo::where('name', 'email_address')->first()->content;
        $facebook = AddressInfo::where('name', 'facebook_link')->first()->content;
        $twitter = AddressInfo::where('name', 'twitter_link')->first()->content;
        $instagram = AddressInfo::where('name', 'instagram_link')->first()->content;
        $linked_in = AddressInfo::where('name', 'linked_in_link')->first()->content;
        $youtube = AddressInfo::where('name', 'youtube_link')->first()->content;
        return view('pages.programme.index', [
            'programmes' => $programs,
            'programme_pretext' => $programme_pretext,
            'address' => $address,
            'postal_address' => $postalAddress,
            'phones' => $phoneNumber,
            'email' => $emailAddress,
            'facebook' => $facebook,
            'twitter' => $twitter,
            'instagram' => $instagram,
            'linked' => $linked_in
//            'youtube' => $youtube,
        ]);
    }

    public function programme($id){
        $programs = Programme::where('active', 1)->get();
        $programs->each(function ($item){
            $item['banner_image'] = asset(Storage::url('programmes/'.$item['programme_image_name'].'_bg.jpg'));
            $item['programme_image_name'] = asset(Storage::url('programmes/'.$item['programme_image_name'].'.jpg'));
        });
        $actualProgramme = $programs->where('id', $id)->first();

        $courses = ProgrammeCourse::select('course_code')->where('programme_id', $id)->get();

        $courses->each(function ($item){
            $item['course_name'] = Course::select('course_name')->where('course_code', $item['course_code'])->first()->course_name;
        });

        //footer shit
        $address = AddressInfo::where('name', 'address')->first()->content;
        $postalAddress = AddressInfo::where('name', 'postal_address')->first()->content;
        $firstPhone = AddressInfo::where('name', 'telephone_1')->first()->content;
        $secondPhone = AddressInfo::where('name', 'telephone_2')->first()->content;
        $phoneNumber = $firstPhone;
        if ($secondPhone !== ''){
            $phoneNumber = $firstPhone.' | '.$secondPhone;
        }
        $emailAddress = AddressInfo::where('name', 'email_address')->first()->content;
        $facebook = AddressInfo::where('name', 'facebook_link')->first()->content;
        $twitter = AddressInfo::where('name', 'twitter_link')->first()->content;
        $instagram = AddressInfo::where('name', 'instagram_link')->first()->content;
        $linked_in = AddressInfo::where('name', 'linked_in_link')->first()->content;
//        $youtube = AddressInfo::where('name', 'youtube_link')->first()->content;
        return view('pages.programme.programme', [
            'programmes' => $programs,
            'actual_programme' => $actualProgramme,
            'courses' => $courses,
            'address' => $address,
            'postal_address' => $postalAddress,
            'phones' => $phoneNumber,
            'email' => $emailAddress,
            'facebook' => $facebook,
            'twitter' => $twitter,
            'instagram' => $instagram,
            'linked' => $linked_in
//            'youtube' => $youtube,
        ]);
    }

    public function board(){
        $programs = Programme::where('active', 1)->get();
        $trustees = TrusteeBoard::all();
        $trustees->each(function ($item){
            $item['image_name'] = asset(Storage::url('board/'.$item['image_name']));
            $item['country_image_name'] = asset('images/'.$item['country_image_name']);
        });
        //footer shit
        $address = AddressInfo::where('name', 'address')->first()->content;
        $postalAddress = AddressInfo::where('name', 'postal_address')->first()->content;
        $firstPhone = AddressInfo::where('name', 'telephone_1')->first()->content;
        $secondPhone = AddressInfo::where('name', 'telephone_2')->first()->content;
        $phoneNumber = $firstPhone;
        if ($secondPhone !== ''){
            $phoneNumber = $firstPhone.' | '.$secondPhone;
        }
        $emailAddress = AddressInfo::where('name', 'email_address')->first()->content;
        $facebook = AddressInfo::where('name', 'facebook_link')->first()->content;
        $twitter = AddressInfo::where('name', 'twitter_link')->first()->content;
        $instagram = AddressInfo::where('name', 'instagram_link')->first()->content;
        $linked_in = AddressInfo::where('name', 'linked_in_link')->first()->content;
//        $youtube = AddressInfo::where('name', 'youtube_link')->first()->content;
        return view('pages.staff.trustees', [
            'programmes' => $programs,
            'trustees' => $trustees,
            'address' => $address,
            'postal_address' => $postalAddress,
            'phones' => $phoneNumber,
            'email' => $emailAddress,
            'facebook' => $facebook,
            'twitter' => $twitter,
            'instagram' => $instagram,
            'linked' => $linked_in
//            'youtube' => $youtube,
        ]);
    }

    public function team(){
        $programs = Programme::where('active', 1)->get();
        $cores = TrusteeBoard::all();
        $faculty_pretext = SchoolInfo::where('name', 'faculty_pretext')->first()->content;
        $authorization = SchoolInfo::where('name', 'authorization')->first()->content;
        $affiliation = SchoolInfo::where('name', 'affiliation')->first()->content;
        $faculty = Faculty::all();

        //footer shit
        $address = AddressInfo::where('name', 'address')->first()->content;
        $postalAddress = AddressInfo::where('name', 'postal_address')->first()->content;
        $firstPhone = AddressInfo::where('name', 'telephone_1')->first()->content;
        $secondPhone = AddressInfo::where('name', 'telephone_2')->first()->content;
        $phoneNumber = $firstPhone;
        if ($secondPhone !== ''){
            $phoneNumber = $firstPhone.' | '.$secondPhone;
        }
        $emailAddress = AddressInfo::where('name', 'email_address')->first()->content;
        $facebook = AddressInfo::where('name', 'facebook_link')->first()->content;
        $twitter = AddressInfo::where('name', 'twitter_link')->first()->content;
        $instagram = AddressInfo::where('name', 'instagram_link')->first()->content;
        $linked_in = AddressInfo::where('name', 'linked_in_link')->first()->content;
//        $youtube = AddressInfo::where('name', 'youtube_link')->first()->content;
        return view('pages.staff.team', [
            'programmes' => $programs,
            'cores' => $cores,
            'faculty_pretext' => $faculty_pretext,
            'authorization' => $authorization,
            'affiliation' => $affiliation,
            'faculties' => $faculty,
            'address' => $address,
            'postal_address' => $postalAddress,
            'phones' => $phoneNumber,
            'email' => $emailAddress,
            'facebook' => $facebook,
            'twitter' => $twitter,
            'instagram' => $instagram,
            'linked' => $linked_in
//            'youtube' => $youtube,
        ]);
    }

    public function contact(){
        $programs = Programme::where('active', 1)->get();
        $contact_title = AddressInfo::where('name', 'contact_title')->first()->content;
        $contact_pretext = AddressInfo::where('name', 'contact_pretext')->first()->content;
        //footer shit
        $address = AddressInfo::where('name', 'address')->first()->content;
        $postalAddress = AddressInfo::where('name', 'postal_address')->first()->content;
        $firstPhone = AddressInfo::where('name', 'telephone_1')->first()->content;
        $secondPhone = AddressInfo::where('name', 'telephone_2')->first()->content;
        $phoneNumber = $firstPhone;
        if ($secondPhone !== ''){
            $phoneNumber = $firstPhone.' | '.$secondPhone;
        }
        $emailAddress = AddressInfo::where('name', 'email_address')->first()->content;
        $facebook = AddressInfo::where('name', 'facebook_link')->first()->content;
        $twitter = AddressInfo::where('name', 'twitter_link')->first()->content;
        $instagram = AddressInfo::where('name', 'instagram_link')->first()->content;
        $linked_in = AddressInfo::where('name', 'linked_in_link')->first()->content;
//        $youtube = AddressInfo::where('name', 'youtube_link')->first()->content;
        return view('pages.contact', [
            'programmes' => $programs,
            'contact_title' => $contact_title,
            'contact_pretext' => $contact_pretext,
            'address' => $address,
            'postal_address' => $postalAddress,
            'phones' => $phoneNumber,
            'email' => $emailAddress,
            'facebook' => $facebook,
            'twitter' => $twitter,
            'instagram' => $instagram,
            'linked' => $linked_in
//            'youtube' => $youtube,
        ]);
    }

    public function postContact(Request $request){
        $validatedData = $request->validate([
            'name'            => 'required',
            'email'           => 'bail|required|email',
            'subject'         => 'required',
            'message'         => 'bail|required|max:500',
            'captcha'         => 'bail|required|captcha',
        ]);
        try{
            Mail::send(new Contact($request));
            Alert::success('Message Sent Successfully', 'Success');
        }catch (Exception $exception){
            Alert::error('Message Not Sent. Please try again later.', 'Error');
        }
        return Redirect::contact();
    }
}
