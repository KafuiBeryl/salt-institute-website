<?php

namespace App\Http\Controllers;

use App\Mail\FormSubmission;
use App\Models\AddressInfo;
use App\Models\Programme;
use Illuminate\Http\Request;
use Alert;
use Illuminate\Support\Facades\Mail;

class CourseRegistrationController extends Controller
{
    public function courseRegistration2019Feb(){
        $programs = Programme::select('id','programme_name')->where('active', 1)->get();
        $address = AddressInfo::where('name', 'address')->first()->content;
        $postalAddress = AddressInfo::where('name', 'postal_address')->first()->content;
        $firstPhone = AddressInfo::where('name', 'telephone_1')->first()->content;
        $secondPhone = AddressInfo::where('name', 'telephone_2')->first()->content;
        $phoneNumber = $firstPhone;
        if ($secondPhone !== ''){
            $phoneNumber = $firstPhone.' | '.$secondPhone;
        }
        $emailAddress = AddressInfo::where('name', 'email_address')->first()->content;
        $facebook = AddressInfo::where('name', 'facebook_link')->first()->content;
        $twitter = AddressInfo::where('name', 'twitter_link')->first()->content;
        $instagram = AddressInfo::where('name', 'instagram_link')->first()->content;
        $linked_in = AddressInfo::where('name', 'linked_in_link')->first()->content;
//        $youtube = AddressInfo::where('name', 'youtube_link')->first()->content;
        return view('pages.course.registration', [
            'programmes' => $programs,
            'address' => $address,
            'postal_address' => $postalAddress,
            'phones' => $phoneNumber,
            'email' => $emailAddress,
            'facebook' => $facebook,
            'twitter' => $twitter,
            'instagram' => $instagram,
            'linked' => $linked_in
//            'youtube' => $youtube,
        ]);
    }

    public function forSubmission2019Feb(Request $request){
        $this->validate(request(),[
            'tin_number' => 'bail|required',
            'name' => 'bail|required',
            'gender' => 'bail|required',
            'title'  => 'bail|required',
            'organization' => 'bail|required',
            'position' => 'bail|required',
            'duties' => 'bail|required',
            'address' => 'bail|required',
            'phone' => 'bail|required',
            'email' => 'bail|required|email',
            'course' => 'bail|required',
            'preferred_schedule' => 'bail|required',
            'qualification' => 'bail|required',
            'goal1' => 'bail|required',
            'goal2' => 'bail|required',
            'goal3' => 'bail|required',
            'expectations1' => 'bail|required',
            'expectations2' => 'bail|required',
            'expectations3' => 'bail|required',
            'contribute1' => 'bail|required',
            'contribute2' => 'bail|required',
            'contribute3' => 'bail|required'
        ]);

        try{
            Mail::to('kafui.obiatey@nextinfotechgh.com')->send(new FormSubmission($request));
            Alert::success('Form Submitted Successfully', 'Success');
            return back();
        }catch (\Exception $exception){
            Alert::error('Form Not Submitted', 'Error');
            \Session::flash('message', $exception->getMessage());
            return back();
        }
//        return new FormSubmission($request);
    }
}
