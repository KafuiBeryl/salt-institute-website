<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 06 Aug 2018 17:09:11 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class CoreManagement
 * 
 * @property int $id
 * @property string $title
 * @property string $first_name
 * @property string $last_name
 * @property string $position
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class CoreManagement extends Eloquent
{
	protected $table = 'core_management';

	protected $fillable = [
		'title',
		'first_name',
		'last_name',
		'position'
	];
}
