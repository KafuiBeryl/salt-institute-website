<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 03 Aug 2018 14:36:59 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Quote
 * 
 * @property int $id
 * @property string $quote
 * @property string $speaker
 * @property string $speaker_position
 * @property string $slider_image_name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class Quote extends Eloquent
{
	protected $fillable = [
		'quote',
		'speaker',
		'speaker_position',
        'slider_image_name'
	];
}
