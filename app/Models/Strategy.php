<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 06 Aug 2018 14:08:03 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Strategy
 * 
 * @property int $id
 * @property string $content
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class Strategy extends Eloquent
{
	protected $fillable = [
		'content'
	];
}
