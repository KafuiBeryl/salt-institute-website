<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 03 Aug 2018 14:36:59 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class SchoolInfo
 * 
 * @property int $id
 * @property string $name
 * @property string $content
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class SchoolInfo extends Eloquent
{
	protected $table = 'school_info';

	protected $fillable = [
		'name',
		'content'
	];
}
