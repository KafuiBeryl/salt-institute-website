<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 03 Aug 2018 20:49:17 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Course
 * 
 * @property int $id
 * @property string $course_name
 * @property string $course_code
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $programmes
 *
 * @package App\Models
 */
class Course extends Eloquent
{
	protected $fillable = [
		'course_name',
		'course_code'
	];

	public function programmes()
	{
		return $this->belongsToMany(Programme::class, 'programme_course', 'course_code')
					->withPivot('id')
					->withTimestamps();
	}
}
