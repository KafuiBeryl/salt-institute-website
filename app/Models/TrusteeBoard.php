<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 03 Aug 2018 14:37:00 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class TrusteeBoard
 * 
 * @property int $id
 * @property string $title
 * @property string $first_name
 * @property string $last_name
 * @property string $country_image_name
 * @property string $country
 * @property string $position
 * @property string $image_url
 * @property string $description
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class TrusteeBoard extends Eloquent
{
	protected $table = 'trustee_board';

	protected $fillable = [
		'title',
		'first_name',
		'last_name',
		'country_image_name',
		'country',
		'position',
		'image_name',
		'description'
	];
}
