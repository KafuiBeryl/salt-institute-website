<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 03 Aug 2018 20:48:47 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Programme
 * 
 * @property int $id
 * @property string $programme_name
 * @property string $programme_blurb
 * @property string $programme_image_name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $courses
 *
 * @package App\Models
 */
class Programme extends Eloquent
{
    protected $casts = [
        'active' => 'bool'
    ];
	protected $fillable = [
		'programme_name',
		'programme_blurb',
		'programme_image_name',
        'active'
	];

	public function courses()
	{
		return $this->belongsToMany(Course::class, 'programme_course', 'programme_id', 'course_code')
					->withPivot('id')
					->withTimestamps();
	}
}
