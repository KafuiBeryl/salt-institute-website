<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 03 Aug 2018 14:36:59 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Faculty
 * 
 * @property int $id
 * @property string $title
 * @property string $first_name
 * @property string $last_name
 * @property string $suffix
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class Faculty extends Eloquent
{
	protected $table = 'faculty';

	protected $fillable = [
		'title',
		'first_name',
		'last_name',
		'suffix'
	];
}
