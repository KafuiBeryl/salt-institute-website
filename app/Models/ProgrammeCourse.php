<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 03 Aug 2018 20:47:48 +0000.
 */

namespace App\Models;

use App\Models\Course;
use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ProgrammeCourse
 * 
 * @property int $id
 * @property int $programme_id
 * @property string $course_code
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property Course $course
 * @property \App\Models\Programme $programme
 *
 * @package App\Models
 */
class ProgrammeCourse extends Eloquent
{
	protected $table = 'programme_course';

	protected $casts = [
		'programme_id' => 'int'
	];

	protected $fillable = [
		'programme_id',
		'course_code'
	];

	public function course()
	{
		return $this->belongsTo(Course::class, 'course_code', 'course_code');
	}

	public function programme()
	{
		return $this->belongsTo(Programme::class);
	}
}
