<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Http\Request;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Contact extends Mailable
{
    use Queueable, SerializesModels;


    protected $message;

    /**
     * Create a new message instance.
     * @param Request $request
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->message = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('noreply@saltinstituteghana.org',$this->message->name)
            ->with(['message' => $this->message])
            ->subject('Contact from company website - '. $this->subject)
            ->markdown('emails.contact');
    }
}
