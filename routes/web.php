<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/cms', 'CMSHomeController@index')->name('home.cms');

Route::get('/about', 'HomeController@about')->name('about');

Route::get('/programme', 'HomeController@programLanding')->name('programme');

Route::get('/programme/{id}', 'HomeController@programme')->name('single.programme');

Route::get('/board', 'HomeController@board')->name('board');

Route::get('/team', 'HomeController@team')->name('team');

Route::get('/contact', 'HomeController@contact')->name('contact');

Route::post('/contact', 'HomeController@contact')->name('contact.post');

Route::get('/course/registration', 'CourseRegistrationController@courseRegistration2019Feb')->name('course.registration');

Route::post('/course/registration/submit', 'CourseRegistrationController@forSubmission2019Feb')->name('course.registration.submit');
